const Hapi = require("@hapi/hapi");
const Inert = require("@hapi/inert");

const init = async () => {
  const server = Hapi.server({
    port: process.env.PORT || 3000,
    host: "0.0.0.0", // Ensures compatibility on Heroku
  });

  await server.register(Inert);

  server.ext("onRequest", (request, h) => {
    // Identify if running in a local development environment
    const isLocalhost =
      request.headers.host.startsWith("localhost") ||
      request.headers.host.startsWith("127.0.0.1");
    const isHerokuAppDomain = request.headers.host.endsWith(".herokuapp.com");
    const isHttp =
      request.headers["x-forwarded-proto"] &&
      request.headers["x-forwarded-proto"] === "http";
    const isNonWww = !request.headers.host.startsWith("www.");

    if (!isLocalhost) {
      if (isHttp) {
        return h
          .redirect(`https://${request.headers.host}${request.path}`)
          .code(301)
          .takeover();
      }

      if (!isHerokuAppDomain && isNonWww) {
        const newHost = `www.${request.headers.host}`;
        return h
          .redirect(`https://${newHost}${request.path}`)
          .code(301)
          .takeover();
      }
    }

    return h.continue;
  });

  server.route({
    method: "GET",
    path: "/{param*}",
    handler: {
      directory: {
        path: "build",
        redirectToSlash: true,
        index: true,
      },
    },
  });

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", (err) => {
  console.log("unhandledRejection", err);
  process.exit(1);
});

init();
