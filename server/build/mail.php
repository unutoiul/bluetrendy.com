<?php
	header("Content-Type:application/json");

	$data = json_decode(file_get_contents('php://input'), true);

	//Set Static Vars
	$emailto = "contact@bluetrendy.com";
	$pageURL = $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];

	//Form vars
	$firstname = $data['firstname'];
	$lastname = $data['lastname'];
	$name = $data['name'];
	$secret = $data["secret"];
	$phone = $data["phone"];
	$emailfrom = $data["email"];
	$message = $data["message"];
	$subject = $data["subject"];

	// user infomation
	$ip = $_SERVER['REMOTE_ADDR'];

	if($secret != "X9gV1d8zWsMM"){ 
		return jsonResponse(400, "Secret Key Error", $data);
	}

	if(	!empty($firstname) && !empty($lastname) && !empty($phone) && !empty($emailfrom) && !empty($message) && !empty($subject)) {
		sendEmail($ip, $firstname, $lastname, $emailto, $emailfrom, $phone, $message, $data, $subject);
	} else {
		return jsonResponse(400, "Invalid Request", $data);
	}
	
	
	function sendEmail($ip, $firstname, $lastname, $emailto, $emailfrom, $phone, $message, $data, $subject){
		$mailheader = "From: " .$emailfrom."\r\n";
		$mailheader .= "Reply-To: ".$emailfrom."\r\n";
		$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n";
		
		$message_body = "<br/><b>Name:</b> ".$firstname." ".$lastname."<br/>";
		$message_body .= "<b>Phone: </b> ".$phone."<br/>";
		$message_body .= "<b>Email: </b> ".$emailfrom."<br/>";
		$message_body .= "<b>Message: </b> ".$message."<br/>";
			
		$message_body .= "<br/><b>User IP Address:</b>  ".$ip. "<br/><br/>";
		$subject = $name . " was contacting you using the BlueTrendy.com contact form regarding" . $subject;


		if(mail($emailto, $subject, $message_body, $mailheader)) {
			jsonResponse(200, "Mail was send succesfully.", $data);
	   	}else {
			jsonResponse(400, "Mail wasn't send succesfully.", $data);
	   }
	}

	function jsonResponse($status,$status_message,$data) {
		header("HTTP/1.1 ".$status_message);
		$response['status']=$status;
		$response['status_message']=$status_message;
		$response['data']=$data;
		$json_response = json_encode($response);
		echo $json_response;
	}
?> 