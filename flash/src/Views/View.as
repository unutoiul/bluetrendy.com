package Views {
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Views.Base.CompositeViewBase;
	import Views.Components.Background;
	import Views.Components.Footer;
	import Views.Components.Logo;
	import Views.Components.MainLoader;
	import Views.Components.Menu;

	import com.greensock.TweenMax;

	/**
	 * @author LucasOK
	 */
	public class View extends CompositeViewBase{
		private var mBackground 		: Background;
		private var mLogo 				: Logo;
		private var mLoader 			: MainLoader;
		private var mMenu 				: Menu;
		private var mFooter 			: Footer;

		private var mPages		 		: Pages;
		
		public function View(arg_model: Model):void {
			super(arg_model);
			oModel = arg_model;
			
			iniListeners();
			ini();
		}

		private function iniListeners() : void {
			CustomSignals.WEBSITE_LOADED.add(fStartWebsite);
		}

		public function fShowLoader(arg_b :Boolean) : void {
			if(arg_b){
				mLoader = new MainLoader(oModel, oController);
				addChild(mLoader);			
			}else{
				removeChild(mLoader);			
			}
		}

		private function ini() : void {
			oController = new Controller(oModel, this);
			mBackground = new Background(oModel, oController);
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			addChild(mBackground);
		}
		
		private function fStartWebsite() : void {
			mMenu = new Menu(oModel, oController);
			mFooter = new Footer(oModel, oController);
			mLogo = new Logo();
			mPages = new Pages(oModel, oController);
			
			CustomSignals.WEBSITE_LOADED.remove(fStartWebsite);
			CustomSignals.PAGE_CHANGE.add(fChangePage);
			CustomSignals.STAGE_RESIZE.add(fPositions);

			fShowLoader(false);
			
			TweenMax.from(mMenu, 1, {alpha:0});
			TweenMax.from(mLogo, 1, {alpha:0});
			
			//Main Page
			addChild(mPages);
			addChild(mLogo);
			addChild(mMenu);
			addChild(mFooter);
			
			fPositions();
		}

		private function fPositions() : void {
			mMenu.y = BlueTrendyStaticList.APP_MARGIN;
			mMenu.x = BlueTrendyStaticList.APP_MARGIN;

			mLogo.x = BlueTrendyStaticList.APP_WIDTH - mLogo.iconMC.width -BlueTrendyStaticList.APP_MARGIN;
			mLogo.y = BlueTrendyStaticList.APP_MARGIN;
			
			mFooter.x = BlueTrendyStaticList.APP_MARGIN;
			mFooter.y = BlueTrendyStaticList.APP_HEIGHT - mFooter.height - BlueTrendyStaticList.APP_MARGIN;
			
			mBackground.x = BlueTrendyStaticList.APP_WIDTH/2 - mBackground.width/2;
			
			mPages.fPositionPage();
		}
		
		private function fChangePage(arg_s :String) : void {
			mPages.fChangePage(arg_s);
		}

//		private function fButtonsEnabled(arg_b :Boolean) : void {
//			CustomSignals.BUTTONS_ENABLED.dispatch(arg_b);
//		}
	}
}
