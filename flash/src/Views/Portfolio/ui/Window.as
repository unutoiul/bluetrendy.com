package Views.Portfolio.ui {
	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.VO.ImageVO;
	import Models.VO.WindowVO;

	import Utils.ApplicationFonts;
	import Utils.Buttons;

	import com.greensock.TweenLite;
	import com.greensock.TweenMax;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.net.navigateToURL;
	import flash.text.StyleSheet;
	import flash.text.TextField;
	import flash.text.TextFormat;
	
	/**
	 * @author LucasOK
	 */
	public class Window extends MovieClip {
		private var nId 				: int;
		private var nCurrentBlur 		: int;
		private var nCurrentImage 		: int;
		private var nNextImage 			: int;
		private var nLastImage 			: int;
		private var nAngleProgress 		: Number;
		
		private var mNav 				: MovieClip;
		private var mMask 				: MovieClip;
		private var mImage 				: MovieClip;
		private var mInfo 				: MovieClip;
		
		//Assets
		private var mNextBT 			: NavButton;
		private var mBackBT 			: NavButton;
		private var mMiniLoader 		: miniLoaderMC;
		private var mThumb 				: Thumb;
		private var mInfoBT 			: MovieClip;
		private var mLaunchBT 			: MovieClip;
		private var mGridBT 			: MovieClip;
		
		private var oWindowVO 			: WindowVO;
		private var aIcons 				: Array;
		private var aImages 			: Array;
		private var bShowInfo 			: Boolean;
		private var mCloseBT 			: closeBT;

		public function Window(arg_array : WindowVO) : void {
			// constructor
			oWindowVO = arg_array;
			ini();
		}

		private function ini() : void {
			aIcons = new Array();
			aImages = new Array();
			
			
			mBackBT = new NavButton(new navBT());
			mNextBT = new NavButton(new navBT());
			
			mInfoBT = Buttons.fCreateNormalButton(30, 30, 100, BlueTrendyStaticList.COLOR_BLUE, "i");
			mLaunchBT = Buttons.fCreateNormalButton(100, 25, 25, BlueTrendyStaticList.COLOR_BLUE, "LAUNCH");
			mGridBT = Buttons.fCreateNormalButton(40, 25, 25, BlueTrendyStaticList.COLOR_GREY_DARK, "", new gridSymbol());

			mThumb = new Thumb((oWindowVO.images[0] as ImageVO).thumb, oWindowVO.name, oWindowVO.shortDesc);

			fAddToStage();
		}

		private function fAddToStage() : void {
			addChild(mThumb);
			
			fEnable(false);
			fEnableThumb(true);
		}
		
		public function fShowNav(arg_b : Boolean) : void {
			if (arg_b) {
				fCreateItems();
				fCreateText();
				fCreateNav();
				fCreateInfo(true);
			} else {
				fRemoveItems();
				fCreateInfo(false);
			}
		}
		

		private function fCreateNav() : void {
			mNextBT.rotation = 180;

			mBackBT.y = mBackBT.height / 2;
			mBackBT.x = -BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH / 2 - BlueTrendyStaticList.PORTFOLIO_NAV_MARGIN;

			mNextBT.y = mBackBT.height / 2;
			
			mNextBT.x = BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH / 2 + BlueTrendyStaticList.PORTFOLIO_NAV_MARGIN;

			TweenLite.from(mBackBT, .5, {alpha:0});
			TweenLite.from(mNextBT, .5, {alpha:0});

			addChild(mBackBT);
			addChild(mNextBT);
			
			fEnableNav(true);
		}
		
		private function fBackImage(event : MouseEvent) : void {
			var t_n : int = nCurrentImage;
			t_n--;
			if (t_n < 0) t_n = aImages.length - 1;
			fSwapImage(t_n);
		}

		private function fNextImage(event : MouseEvent) : void {
			var t_n : int = nCurrentImage;
			t_n++;
			if (t_n == aImages.length) t_n = 0;
			fSwapImage(t_n);
		}

		
		public function fEnableNav(arg_b : Boolean) : void {
			if (arg_b) {
				mNextBT.addEventListener(MouseEvent.MOUSE_DOWN, fNextImage);
				mBackBT.addEventListener(MouseEvent.MOUSE_DOWN, fBackImage);
			} else {
				mNextBT.removeEventListener(MouseEvent.MOUSE_DOWN, fNextImage);
				mBackBT.removeEventListener(MouseEvent.MOUSE_DOWN, fBackImage);
			}
			
			for (var i : int = 0; i < aIcons.length; i++) {
				if(i==nCurrentImage){
					(aIcons[i] as Icon).fEnabled(false);
					(aIcons[i] as Icon).fSelect(true);
				}else{
					(aIcons[i] as Icon).fEnabled(arg_b);
					(aIcons[i] as Icon).fSelect(false);
				}
			}
		}

		public function fEnable(arg_b : Boolean) : void {
			if (arg_b) {
				addEventListener(MouseEvent.MOUSE_DOWN, fWindowClick);
			} else {
				removeEventListener(MouseEvent.MOUSE_DOWN, fWindowClick);
			}
		}

		public function fShowThumbInfo(arg_b : Boolean) : void {
			mThumb.fEnableInfo(arg_b);	
		}
		
		public function fEnableTimer(arg_b : Boolean) : void {
			if(arg_b){
				TweenLite.delayedCall(BlueTrendyStaticList.PORTFOLIO_SWAP_TIME, fSwapImage);
			}else{
				TweenLite.killTweensOf(fSwapImage);
			}
		}
		
		private function fEnableInfo(arg_b : Boolean) : void {
			if(arg_b){
				mInfoBT.addEventListener(MouseEvent.MOUSE_DOWN, fShowInfo);
				mLaunchBT.addEventListener(MouseEvent.MOUSE_DOWN, fLaunch);
				mGridBT.addEventListener(MouseEvent.MOUSE_DOWN, fGridFormat);
				mCloseBT.addEventListener(MouseEvent.MOUSE_DOWN, fShowInfo);
				mCloseBT.addEventListener(MouseEvent.MOUSE_OVER, fCloseOver);
				mCloseBT.addEventListener(MouseEvent.MOUSE_OUT, fCloseOut);
			}else{
				mInfoBT.removeEventListener(MouseEvent.MOUSE_DOWN, fShowInfo);
				mLaunchBT.removeEventListener(MouseEvent.MOUSE_DOWN, fLaunch);
				mGridBT.removeEventListener(MouseEvent.MOUSE_DOWN, fGridFormat);
				mCloseBT.removeEventListener(MouseEvent.MOUSE_DOWN, fShowInfo);
				mCloseBT.removeEventListener(MouseEvent.MOUSE_OVER, fCloseOver);
				mCloseBT.removeEventListener(MouseEvent.MOUSE_OUT, fCloseOut);
			}
		}

		private function fLaunch(event: MouseEvent) : void {
			var t_sAddress :String = oWindowVO.link;
			
			var t_URLtarget:URLRequest = new URLRequest(t_sAddress);
    		navigateToURL(t_URLtarget, "_blank");
		}

		private function fGridFormat(event:MouseEvent) : void {
			CustomSignals.PORTFOLIO_GRID_FORMAT.dispatch();
		}

		private function fCloseOver(event :MouseEvent) : void {
			TweenMax.to(mCloseBT, .5, {rotation:270, alpha:1}); 
		}
		
		private function fCloseOut(event :MouseEvent) : void {
			TweenMax.to(mCloseBT, .5, {rotation:0, alpha:.3}); 
		}
		
		private function fCreateInfo(arg_b:Boolean):void{
			if(arg_b){	
				var t_tFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, 11, 0x000000);
				var t_tTextField :TextField = ApplicationFonts.fCreateDynamicText(t_tFormat);
				var t_sStyle:StyleSheet = new StyleSheet();

				var t_sFirstLetter :String = oWindowVO.desc.substr(0, 1);
				var t_sDesc :String = oWindowVO.desc.substr(1,  oWindowVO.desc.length);
				var t_sClient :String = "<b><span class='large'>C</span>lient:</b> " + oWindowVO.client;
				var t_sTechnologies :String = "<b><span class='large'>T</span>echnologies:</b> " + oWindowVO.technologies;
				var t_sProductionTime :String = "<b><span class='large'>P</span>roduction Time:</b> " + oWindowVO.productionTime;

			 	mInfo = new MovieClip();
				mCloseBT = new closeBT();
				
				t_sStyle.parseCSS(".capitalize{float: left; fontSize:46; line-height: 1; fontWeight:bold; margin-right: 0.2;}.large{fontSize:16;}");
				
				t_tTextField.styleSheet = t_sStyle;

				t_tTextField.htmlText = "<span class='capitalize'>"+t_sFirstLetter+"</span>" + t_sDesc + "<br/>" + t_sClient+ "<br/>" + t_sTechnologies+ "<br/>" + t_sProductionTime;
				
				mInfo.graphics.beginFill(BlueTrendyStaticList.COLOR_GREY_VERY_LIGHT, 1);
				mInfo.graphics.lineStyle(1, BlueTrendyStaticList.COLOR_GREY_VERY_LIGHT);
				mInfo.graphics.drawRect(0, 0, BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH, BlueTrendyStaticList.PORTFOLIO_IMAGE_HEIGHT);
				mInfo.graphics.endFill();
				
				t_tTextField.width = BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH - BlueTrendyStaticList.PORTFOLIO_INFOTEXT_MARGIN*2;
				
				mInfo.x = -BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH/2;
				mInfo.y = -BlueTrendyStaticList.PORTFOLIO_IMAGE_HEIGHT/2; 

				t_tTextField.y = BlueTrendyStaticList.PORTFOLIO_INFOTEXT_DESC_Y;
				t_tTextField.x = BlueTrendyStaticList.PORTFOLIO_INFOTEXT_MARGIN;
				
				mCloseBT.x = BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH - mCloseBT.width/2 - BlueTrendyStaticList.PORTFOLIO_INFOTEXT_MARGIN;
				mCloseBT.y = BlueTrendyStaticList.PORTFOLIO_INFOTEXT_MARGIN + mCloseBT.height/2;
				mCloseBT.alpha = .3;
				
				mInfo.alpha = 0;
				mInfo.visible = false;
				
				mInfo.addChild(mCloseBT);
				mInfo.addChild(t_tTextField);
				addChild(mInfo);
				fEnableInfo(true);
			}else{
				try{
					removeChild(mInfo);
					mInfo = null;
					fEnableInfo(false);
				}catch(error:Error){
					trace("Info not on stage");						
				}
			}
		}

		private function fShowInfo(event :MouseEvent = null) : void {
			bShowInfo = !bShowInfo;
			if(bShowInfo){
				TweenMax.to(mInfo, .5,{autoAlpha:1});
				fEnableTimer(false);
			}else{
				TweenMax.to(mInfo, 1,{autoAlpha:0});
				fEnableTimer(true);
			}
		}
		

		private function fWindowClick(event : MouseEvent) : void {
			CustomSignals.PORTFOLIO_THUMB_CLICK.dispatch(this);
		}
		
		private function fThumbOut(event : MouseEvent = null) : void {
			TweenLite.to(this, .5, {blurFilter:{blurX:nCurrentBlur, blurY:nCurrentBlur}, colorMatrixFilter:{saturation:0}});
		}

		private function fThumbOver(event : MouseEvent = null) : void {
			TweenLite.to(this, .7, {blurFilter:{blurX:0, blurY:0}, colorMatrixFilter:{saturation:1}});
		}

		private function fCreateText() : void {
			var t_tTitleFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, 10, 0x000000, "left", true);
			var t_tDescFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, 10, 0x333333);
			var t_tTitleField :TextField = ApplicationFonts.fCreateText(t_tTitleFormat);
			var t_tDescField :TextField = ApplicationFonts.fCreateText(t_tDescFormat);
			
			t_tTitleField.text = oWindowVO.name;
			t_tDescField.text = oWindowVO.shortDesc;
			
			t_tTitleField.x = BlueTrendyStaticList.PORTFOLIO_MARGIN;
			t_tDescField.x = BlueTrendyStaticList.PORTFOLIO_MARGIN;
			
			t_tTitleField.y = 5;
			t_tDescField.y = 18;
			
			TweenLite.from(t_tTitleField, 1, {alpha:0});
			TweenLite.from(t_tDescField, 1, {alpha:0});
			
			mNav.addChild(t_tTitleField);
			mNav.addChild(t_tDescField);
		}

		public function fEnableThumb(arg_b : Boolean) : void {
			if(arg_b){
				addEventListener(MouseEvent.MOUSE_OVER, fThumbOver);
				addEventListener(MouseEvent.MOUSE_OUT, fThumbOut);
			}else{
				removeEventListener(MouseEvent.MOUSE_OVER, fThumbOver);
				removeEventListener(MouseEvent.MOUSE_OUT, fThumbOut);
				fThumbOver();
			}
			mThumb.fEnabled(arg_b);
		}
		
		public function fShowThumb(arg_s : String) : void {
			mThumb.fShow(arg_s);
		}

		public function fShowImage(arg_b : Boolean) : void {
			if (arg_b) {
				addChildAt(mImage, 0);
				(aImages[nCurrentImage] as Image).fLoad(true);
			} else {
				for (var i : int = 0; i < aImages.length; i++) {
					TweenLite.killTweensOf((aImages[i] as Image));
					(aImages[i] as Image).fLoad(false);
//					aImages[i] = null;
				}
				
				try{ removeChild(mImage); mImage = null;
				}catch(error:Error){trace("Image not on stage!");}
			}
		}

		private function fCreateItems() : void {
			// trace("fCreateItems");
			aIcons = [];
			aImages = [];

			mNav = new MovieClip();
			mImage = new MovieClip();
			mMask = new MovieClip();
			mInfo = new MovieClip();
			mMiniLoader = new miniLoaderMC();
			
			nCurrentImage = 0;
			nLastImage = 0;
			
			mMask.graphics.beginFill(0x000000, .2);
			mMask.graphics.drawRect(0, 0, BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH, BlueTrendyStaticList.PORTFOLIO_IMAGE_HEIGHT);
			mMask.graphics.endFill();

			mImage.mask = mMask;
			mNav.graphics.beginFill(0xCCCCCC, .5);
			mNav.graphics.drawRect(0, 0, BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH, BlueTrendyStaticList.PORTFOLIO_ICON_HEIGHT);
			mNav.graphics.endFill();
			
			for (var i : int = 0; i < oWindowVO.images.length; i++) {
				var t_mItem : Icon = new Icon(i, (oWindowVO.images[i] as ImageVO).icon);
				var t_mImage : Image = new Image(i, (oWindowVO.images[i] as ImageVO).src);

				t_mItem.x = BlueTrendyStaticList.PORTFOLIO_ICON_X + BlueTrendyStaticList.PORTFOLIO_ICON_WIDTH * i;

				aIcons.push(t_mItem);
				aImages.push(t_mImage);

				mNav.addChild(t_mItem);
			}
			
			// Default Select
			mImage.addChild(aImages[nCurrentImage]);
//			(aIcons[nCurrentImage] as Icon).fSelect(true);
//			(aIcons[nCurrentImage] as Icon).fEnabled(false);
			
			//Add to VIEW
			mNav.addChild(mInfoBT);
			mNav.addChild(mLaunchBT);
			mNav.addChild(mGridBT);
			addChild(mMask);
			addChild(mNav);
			addChild(mMiniLoader);
			fPosNav();
		}

		private function fPosNav() : void {
			mMask.y = -mMask.height / 2;
			mMask.x = -mMask.width / 2;

			mNav.y = this.height / 2;
			mNav.x = -mNav.width / 2;

			mInfoBT.x = mNav.width - mInfoBT.width - BlueTrendyStaticList.PORTFOLIO_MARGIN;
			mInfoBT.y = mNav.height/2 - mInfoBT.height/2;

			mLaunchBT.x = mInfoBT.x - mLaunchBT.width - BlueTrendyStaticList.PORTFOLIO_MARGIN;
			mLaunchBT.y = mNav.height/2 - mLaunchBT.height/2;
			
			mGridBT.x = mLaunchBT.x - mGridBT.width - BlueTrendyStaticList.PORTFOLIO_MARGIN;
			mGridBT.y = mNav.height/2 - mLaunchBT.height/2;
		}

		private function fRemoveItems() : void {
			if(mNav != null){
				for (var i : int = 0; i < mNav.numChildren; i++) {
					mNav.removeChildAt(i);
				}
				removeChild(mMask);
				removeChild(mBackBT);
				removeChild(mNextBT);
				removeChild(mNav);
				removeChild(mMiniLoader);
				
				mNav = null;
				
				fEnableNav(false);
			}
		}

		public function fSetProperties(arg_rotation : int = 0, arg_blur : int = 0) : void {
			nCurrentBlur = arg_blur;
			TweenLite.to(this, .5, {rotation:arg_rotation, blurFilter:{blurX:arg_blur, blurY:arg_blur}, colorMatrixFilter:{saturation:0}});
		}

		public function fSwapImage(arg_id : int = -1) : void {
			//Clean up the Loader
			trace("Lu: fSwapImage Window ID: " + nId + " nLastImage: " + nLastImage + "loader: ");
			
			if((aImages[nLastImage] as Image).loader) (aImages[nLastImage] as Image).fLoad(false);
			if(bShowInfo) fShowInfo();
			
			nNextImage = arg_id;
			if (nCurrentImage == nNextImage) return;
			trace("fSwapImage arg_id " + arg_id);
			
//			(aIcons[nNextImage] as Icon).fSelect(true);
//			(aIcons[nLastImage] as Icon).fSelect(false);
			
			if(nNextImage == -1) {
				nNextImage = nCurrentImage;
				nNextImage++; 
				if(nNextImage == aImages.length) nNextImage = 0;
			}
			
			nLastImage = nNextImage;
			
			var t_mNextImage :Image = (aImages[nNextImage] as Image);
			t_mNextImage.swap = true;
			t_mNextImage.fLoad(true);
		}

		public function fStartSwap() : void {
			trace("fStartSwap");
			
			var t_tMc : Image = aImages[nCurrentImage] as Image;
			
			TweenLite.to(t_tMc, 1, {x:-BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH - BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH / 2, onComplete:fRemoveImage, onCompleteParams:[t_tMc]});
			
			nCurrentImage = nNextImage;

			if(mThumb.alpha == 1) {
				aImages[nCurrentImage].x = -BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH/2;
				fShowThumb(BlueTrendyStaticList.PORTFOLIO_THUMB_HIDE);
			}else{
				(aImages[nCurrentImage] as Image).x = BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH / 2;
				TweenLite.to(aImages[nCurrentImage], 1, {x:-BlueTrendyStaticList.PORTFOLIO_IMAGE_WIDTH / 2});
			}
			
			mImage.addChild(aImages[nCurrentImage]);
			fEnableNav(false);
		}
		
		private function fRemoveImage(arg_mc : MovieClip) : void {
			mImage.removeChild(arg_mc);
			fEnableNav(true);
		}
		
		
		public function fShowLoader(arg_b :Boolean) : void {
			mMiniLoader.gotoAndPlay(1);
			mMiniLoader.visible = arg_b;
			fEnableTimer(!arg_b);
		}

		public function get angleProgress() : Number {
			return nAngleProgress;
		}

		public function set angleProgress(arg_n : Number) : void {
			nAngleProgress = arg_n;
		}

		public function get id() : int {
			return nId;
		}

		public function set id(arg_n : int) : void {
			nId = arg_n;
		}
	}
}
