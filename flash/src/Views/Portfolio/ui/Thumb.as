package Views.Portfolio.ui {
	import flash.events.ProgressEvent;
	import com.greensock.easing.Strong;
	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Utils.ApplicationFonts;

	import com.greensock.TweenLite;
	import com.greensock.TweenMax;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author user
	 */
	public class Thumb extends MovieClip{
		private var sThumb 				: String;
		private var sTitle 				: String;
		private var sShortDesc 			: String;
		
		private var tTitle 				: TextField;
		private var tShortDesc 			: TextField;
		
		private var bInfoEnabled 		: Boolean;

		private var mInfo 				: MovieClip;
		private var mTitle 				: MovieClip;
		private var mShortDesc 			: MovieClip;
		private var mTitleBG 			: MovieClip;
		private var mShortDescBG 		: MovieClip;
		
		private var lLoader 			: Loader;
		private var nPercent 			: Number;
		
		public function Thumb(arg_thumb :String, arg_title:String, arg_short_desc :String) : void {
			sThumb = arg_thumb;
			sTitle = arg_title;
			sShortDesc = arg_short_desc;
			
			trace("Lu: Thumb Source: " + sThumb);
			ini();
		}
	
		private function ini() : void {
			mInfo = new MovieClip();
			mShortDesc = new MovieClip();
			mTitle = new MovieClip();
			bInfoEnabled = false;
			
			fAddToStage();
		}
	
		private function fAddToStage() : void {
			fLoad(true);
		}

		public function fLoad(arg_b : Boolean) : void {
			if(arg_b){
				fStartLoad();
			}else{
				fStartUnLoad();
			}
		}
		
		private function fStartUnLoad() : void {
			trace("fStartUnLoad " + sThumb);
//			if(lLoader){	
				lLoader.unloadAndStop();
				lLoader.unload();
//			}
		}

		private function fStartLoad() : void {
			lLoader = new Loader();
			lLoader.load(new URLRequest(sThumb));
			lLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, fLoadThumb);
			lLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, fOnProgress);
			lLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, fLoadError);
			
//			lLoader.visible = false;
//			addChild(lLoader);
		}
		private function fOnProgress(event: ProgressEvent) : void {
			var t_nPercent :Number = Math.round((event.bytesLoaded / event.bytesTotal) * 100);
//			CustomSignals.UPDATE_MAIN_LOADER.dispatch(t_nPercent);    
		}

		public function fEnabled(arg_b : Boolean) : void {
			if(arg_b){
				addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			}else{
				removeEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				removeEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
				fMouseOut(null);
			}
		}

		private function fMouseOut(event : MouseEvent) : void {
			tTitle.textColor = tShortDesc.textColor =  BlueTrendyStaticList.COLOR_GREY_LIGHT;
			TweenLite.to(mTitleBG, .4, {colorTransform:{tint:BlueTrendyStaticList.COLOR_YELLOW, tintAmount:0}});
			TweenLite.to(mShortDescBG, .4, {colorTransform:{tint:BlueTrendyStaticList.COLOR_YELLOW, tintAmount:0}});
			buttonMode = false;
		}
		
		private function fMouseOver(event : MouseEvent) : void {
			tTitle.textColor = tShortDesc.textColor =  BlueTrendyStaticList.COLOR_GREY_DARK;
			TweenLite.to(mTitleBG, 0, {tint:BlueTrendyStaticList.COLOR_YELLOW});
			TweenLite.to(mShortDescBG, 0, {tint:BlueTrendyStaticList.COLOR_YELLOW});
			
			buttonMode = true;
		}

		private function fLoadError(event :IOErrorEvent) : void {
			trace(event.text);
		}

		private function fLoadThumb(event : Event) : void {
			var t_mThumb :Bitmap = Bitmap(event.target.content);
			t_mThumb.smoothing = true;
			addChild(t_mThumb);
			
//			var t_r:MovieClip = new MovieClip();
//			t_r.graphics.beginFill(0x000000);
//			t_r.graphics.drawCircle(0, 0, 10);
//			t_r.graphics.endFill();
//			addChild(t_r);

			t_mThumb.x = -this.width/2;
			t_mThumb.y = -this.height/2;
			
			CustomSignals.PORTFOLIO_THUMB_LOADED.dispatch();
		}

		public function fShow(arg_s:String) : void {
			switch(arg_s){
				case BlueTrendyStaticList.PORTFOLIO_THUMB_HIDE:
				fHide();
				break;
				
				case BlueTrendyStaticList.PORTFOLIO_THUMB_FADEIN:
				fFadeIn();
				break;
				
				case BlueTrendyStaticList.PORTFOLIO_THUMB_FADEOUT:
				fFadeOut();
				break;
			}
		}
		
		public function fCreateInfo() : void {
			var t_fTitle :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, 11, BlueTrendyStaticList.COLOR_GREY_LIGHT);
			var t_fShortDesc :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fStandard55, 8, BlueTrendyStaticList.COLOR_GREY_LIGHT);
			
			tTitle  = ApplicationFonts.fCreateText(t_fTitle);
			tShortDesc  = ApplicationFonts.fCreateText(t_fShortDesc);
			
			tTitle.text = sTitle;
			tShortDesc.text = sShortDesc;
			
			mTitleBG = fCreateSquare(tTitle.width, tTitle.height);
			mShortDescBG = fCreateSquare(tShortDesc.width, tShortDesc.height);
			
			mTitleBG.x = mTitleBG.y = -BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN;
			mShortDescBG.x = mShortDescBG.y = -BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN;

			tShortDesc.mouseEnabled = false;
			tTitle.mouseEnabled = false;
			mTitleBG.mouseEnabled = false;
			mShortDesc.mouseEnabled = false;
			mShortDesc.mouseChildren = false;
			
			mTitle.mouseEnabled = false;
			mTitle.mouseChildren = false;
			
			mTitle.addChild(mTitleBG);
			mTitle.addChild(tTitle);
			mShortDesc.addChild(mShortDescBG);
			mShortDesc.addChild(tShortDesc);
			
			mTitle.x = mShortDesc.x = BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN;
			mShortDesc.y = mTitle.height +BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN;
			
//			TweenLite.to(this, 0, {colorMatrixFilter:{saturation:0}});
			
			mInfo.addChild(mTitle);
			mInfo.addChild(mShortDesc);
		}

		private function fCreateSquare(arg_w :Number, arg_h :Number) : MovieClip {
			var t_m :MovieClip = new MovieClip();
			t_m.graphics.beginFill(BlueTrendyStaticList.COLOR_GREY_DARK);
			t_m.graphics.drawRect(0, 0, arg_w + BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN *2, arg_h + BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN *2);
			t_m.graphics.endFill();
			
			return t_m;
		}
		
		public function fEnableInfo(arg_b:Boolean) : void {
			if(!bInfoEnabled){
				bInfoEnabled = true;
				fCreateInfo();
			}
			
			if(arg_b){
				mInfo.y = BlueTrendyStaticList.PORTFOLIO_THUMB_HEIGHT/2 + BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN*2;
				mInfo.x = -BlueTrendyStaticList.PORTFOLIO_THUMB_WIDTH/2;
				TweenMax.to(mInfo, 1, {autoAlpha:1, ease:Strong.easeIn});
				addChild(mInfo);
			}else{
				TweenMax.to(mInfo, 0, {autoAlpha:0});
				removeChild(mInfo);
			}
		}
		
		private function fHide() : void {
			TweenMax.to(this, BlueTrendyStaticList.PORTFOLIO_ALPHA_BLUR_TIME, {alpha: 0});
		}
		
		private function fFadeIn() : void {
			TweenMax.to(this, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {scaleY:4, scaleX:4});
		}
		
		private function fFadeOut() : void {
			TweenMax.killTweensOf(this);
			alpha = 1;
			TweenMax.to(this, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {scaleY:1, scaleX:1});
		}

		public function get percent() : Number{
			return nPercent;
		}
	}
}
