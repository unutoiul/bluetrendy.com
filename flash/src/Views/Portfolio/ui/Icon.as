package Views.Portfolio.ui {
	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import com.greensock.TweenMax;
	import com.greensock.easing.Elastic;

	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLRequest;

	/**
	 * @author LucasOK
	 */
	public class Icon extends MovieClip {
		private var sSrc : String;
		private var nId : int;

		public function Icon(arg_id :int, arg_src :String) :void {
			sSrc = arg_src;
			nId = arg_id;
//			trace("LU: Icon SRC: " + sSrc);
			ini();
		}

		private function ini() :void {

			fAddToStage();
		}

		private function fAddToStage() :void {
			fEnabled(true);
			fLoadIcon();
			fMouseOut();
		}

		private function fLoadIcon() :void {
			var t_tLoader :Loader = new Loader();
			t_tLoader.load(new URLRequest(sSrc));
			t_tLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, fIconLoaded);
			t_tLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, fLoadError);
		}
		
		private function fLoadError(event :IOErrorEvent) : void {
			trace(event.text);
		}

		private function fIconLoaded(event :Event): void {
			TweenMax.from(event.target.content, 1, {alpha:0, delay:1*nId/10});
			addChild(event.target.content);
		}

		public function fEnabled(arg_b : Boolean) : void {
			trace("fEnabled : " + arg_b);
			if(arg_b){
				addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
				addEventListener(MouseEvent.MOUSE_DOWN, fMouseClick);
				mouseEnabled = true;
			}else{
				removeEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				removeEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
				removeEventListener(MouseEvent.MOUSE_DOWN, fMouseClick);
				mouseEnabled = false;
			}
		}
//
//		public function fDeselect() : void {
//			fEnabled(true);
//		}
//		
		public function fSelect(arg_b: Boolean) : void {
//			fEnabled(arg_b);
			trace("fSelect : " + arg_b);
			if(arg_b){
				fMouseOver();
			}else{
				fMouseOut();
			}
		}
		
		private function fMouseClick(event : MouseEvent) : void {
			trace("fClick" + nId);
			CustomSignals.PORTFOLIO_ICON_CLICK.dispatch(nId);
		}

		private function fMouseOut(event : MouseEvent = null) : void {
			TweenMax.to(this, .5, {colorTransform: {tint:BlueTrendyStaticList.COLOR_BLUE, tintAmount:0.8}});
			buttonMode = false;
//			TweenMax.to(this, .3, {alpha:.5});
		}

		private function fMouseOver(event : MouseEvent = null) : void {
			TweenMax.to(this, .4, {colorTransform: {tint:BlueTrendyStaticList.COLOR_BLUE, tintAmount:0}}); 
			buttonMode = true;
//			TweenMax.to(this, .2, {alpha:1});
		}
	}
}
