package Views.Portfolio.ui {
	import Events.CustomSignals;

	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	/**
	 * @author user
	 */
	public class Image extends MovieClip{
		private var sSrc 			: String;
		private var bSwap 			: Boolean;
		private var bLoader 		: Boolean;
		private var lLoader 		: Loader;
		private var nId 			: int;
		
		public function Image(arg_id: int, arg_src: String) : void {
			sSrc = arg_src;
			nId = arg_id;
			trace("Lu: Image Source: " + sSrc);
			ini();
		}
	
		private function ini() : void {
			lLoader = new Loader();
			
			bSwap = false;
			
			fAddToStage();
		}
	
		private function fAddToStage() : void {
//			addEventListener(Event.REMOVED_FROM_STAGE, fRemoveFromStage);
//			lLoader.visible = false;
			addChild(lLoader);
		}

		public function fLoad(arg_b:Boolean) : void {
			if(arg_b){
				fStartLoad();
			}else{
				fStartUnLoad();
			}
		}
		
		private function fStartLoad() : void {
			trace("fStartLoad " + nId);
			lLoader.load(new URLRequest(sSrc));
			fShowLoader(true);
			fEnableLoader(true);
//			TweenMax.delayedCall(0.1, fImageLoaded, [null]);
		}

		private function fStartUnLoad() : void {
			trace("fStartUnLoad " + nId);
//			if(lLoader){	
				lLoader.unloadAndStop();
				lLoader.unload();
				fEnableLoader(false);
				fShowLoader(false);
//			}
		}
		
		private function fLoadProgress(event	 :ProgressEvent) : void {
			var percent :int = event.bytesLoaded / event.bytesTotal * 100;
//			trace("progress" + percent + "%");
		}
		
		private function fImageLoaded(event : Event) : void {
			trace("fImageLoaded " + sSrc);
			lLoader.removeEventListener(Event.COMPLETE, fImageLoaded);
			lLoader.removeEventListener(IOErrorEvent.IO_ERROR, fLoadError);
			
			this.x = -this.width/2;
			this.y = -this.height/2;
			
			if(bLoader){ CustomSignals.PORTFOLIO_IMAGE_LOADED.dispatch(bSwap);}
			fShowLoader(false);
		}
		
		private function fLoadError(event :IOErrorEvent) : void {
			fEnableLoader(false);
			trace(event.text);
		}
		
		private function fEnableLoader(arg_b :Boolean) : void {
			if(arg_b){
				lLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, fImageLoaded);
				lLoader.contentLoaderInfo.addEventListener(ProgressEvent.PROGRESS, fLoadProgress);
				lLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, fLoadError);
			}else{
				lLoader.contentLoaderInfo.removeEventListener(Event.COMPLETE, fImageLoaded);
				lLoader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, fLoadProgress);
				lLoader.contentLoaderInfo.removeEventListener(IOErrorEvent.IO_ERROR, fLoadError);
			}
		}
		
		private function fShowLoader(arg_b :Boolean) : void {
			trace("fShowLoader " + arg_b +" "+ nId);
			bLoader = arg_b;
			CustomSignals.PORTFOLIO_IMAGE_LOADER.dispatch(arg_b);
		}
		

		public function set swap(arg_b : Boolean) : void {
			bSwap = arg_b;
		}

		public function get loader() : Boolean {
			return bLoader;
		}
	}
}
