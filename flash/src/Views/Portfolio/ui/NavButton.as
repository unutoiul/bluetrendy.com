package Views.Portfolio.ui {
	import Utils.ApplicationFonts;

	import com.greensock.TweenMax;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;

	/**
	 * @author LucasOK
	 */
	public class NavButton extends MovieClip {
		private var mButton : MovieClip;

		public function NavButton(arg_mc : MovieClip) : void {
			mButton = arg_mc;
			ini();
		}

		private function ini() : void {
			fAddToStage();
		}

		private function fAddToStage() : void {
			mButton.overBT.alpha = 0;

			mButton.overBT.mouseChildren = false;
			mButton.overBT.mouseEnabled = false;

//			if (mButton is launchBT) {
//				var t_tWhiteFormat : TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, 10, 0xFFFFFF, "center",false);
//				var t_tBlueFormat : TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, 10, 0x4180AE, "center", false);
//
//				var t_tWhiteTextfield : TextField = ApplicationFonts.fCreateDynamicText(t_tWhiteFormat);
//				var t_tBlueTextfield : TextField = ApplicationFonts.fCreateDynamicText(t_tBlueFormat);
//
//				t_tWhiteTextfield.text = 'LAUNCH PROJECT';
//				t_tBlueTextfield.text = 'LAUNCH PROJECT';
//
//				t_tBlueTextfield.x = mButton.width / 2 - t_tBlueTextfield.width / 2;
//				t_tBlueTextfield.y = mButton.height / 2 - t_tBlueTextfield.height / 2;
//
//				t_tWhiteTextfield.x = mButton.width / 2 - t_tWhiteTextfield.width / 2;
//				t_tWhiteTextfield.y = mButton.height / 2 - t_tWhiteTextfield.height / 2;
//
//				t_tWhiteTextfield.mouseEnabled = false;
//				t_tBlueTextfield.mouseEnabled = false;
//
//				mButton.normalBT.addChild(t_tWhiteTextfield);
//				mButton.overBT.addChild(t_tBlueTextfield);
//			}

			addChild(mButton);
			fEnabled(true);
		}

		private function fEnabled(arg_b : Boolean) : void {
			if (arg_b) {
				addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			} else {
				removeEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				removeEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			}
		}

		private function fMouseOut(event : MouseEvent) : void {
			// var t_mc : MovieClip = event.target as MovieClip;
			// t_mc.overBT.alpha = 0;
			// TweenMax.killTweensOf(mButton.overBT);
			TweenMax.to(mButton.overBT, 0, {alpha:0});
			buttonMode = false;
		}

		private function fMouseOver(event : MouseEvent) : void {
			// var t_mc : MovieClip = event.target.parent as MovieClip;
			TweenMax.killTweensOf(mButton.overBT);
			mButton.overBT.alpha = 1;
			mButton.overBT.x = mButton.overBT.width;
			TweenMax.to(mButton.overBT, .2, {x:0});
			buttonMode = true;
			// TweenMax.from(t_mc.overBT, .5, {x:t_mc.normalBT.width});
		}
	}
}
