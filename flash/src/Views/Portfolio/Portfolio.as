package Views.Portfolio {
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Views.Base.CompositeViewBase;
	import Views.Portfolio.ui.Window;

	import com.greensock.TweenMax;
	import com.greensock.easing.Strong;
	import com.greensock.motionPaths.CirclePath2D;
	import com.greensock.motionPaths.Direction;
	import com.greensock.plugins.CirclePath2DPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.MovieClip;
	import flash.geom.Point;

	/**
	 * @author Lucas Franco
	 */
	public class Portfolio extends CompositeViewBase {
		TweenPlugin.activate([CirclePath2DPlugin]);
		
		private var mContainer 		: MovieClip;
		private var mCircle 		: CirclePath2D;
		private var mCurrentWindow 	: Window;
		
		private var aWindows 		: Array;
		private var oWindowsVO 		: Array;
		private var aFrontWin 		: Array;
		private var aBackWin 		: Array;
		private var aPositions 		: Array;
		
		private var nRadius 		: int;
		private var nAngleView 		: int;
		private var nAngleDist 		: int;
		private var nAngleStart 	: int;
		private var nThumbLoaded 	: int;
		
		private var bGridFormat 	: Boolean;
//		private var point : MovieClip;

		public function Portfolio(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			
//			ini(BlueTrendyStaticList.MENU_WEBSITES_BUTTON);
		}

		private function iniSignals(arg_b :Boolean) : void {
			if(arg_b){
				CustomSignals.PORTFOLIO_THUMB_LOADED.add(fStartAnimate);
				CustomSignals.PORTFOLIO_THUMB_CLICK.add(fWindowSelect);
				CustomSignals.PORTFOLIO_GRID_FORMAT.add(fGridFormat);
				
				CustomSignals.PORTFOLIO_ICON_CLICK.add(fSwapImage);
				
				CustomSignals.PORTFOLIO_IMAGE_LOADER.add(fShowLoader);
				CustomSignals.PORTFOLIO_IMAGE_LOADED.add(fImageLoaded);
			}else{
				CustomSignals.PORTFOLIO_THUMB_LOADED.remove(fStartAnimate);
				CustomSignals.PORTFOLIO_THUMB_CLICK.remove(fWindowSelect);
				CustomSignals.PORTFOLIO_GRID_FORMAT.remove(fGridFormat);
				
				CustomSignals.PORTFOLIO_ICON_CLICK.remove(fSwapImage);
				
				CustomSignals.PORTFOLIO_IMAGE_LOADER.remove(fShowLoader);
				CustomSignals.PORTFOLIO_IMAGE_LOADED.remove(fImageLoaded);
			}
		}
		
		public function ini(arg_s :String) : void {
			aWindows = new Array();
			aPositions = new Array();
			
			aFrontWin = new Array();
			aBackWin = new Array();
			
			mContainer = new MovieClip();

			nThumbLoaded = 0;
			
			nRadius = BlueTrendyStaticList.PORTFOLIO_CIRCLE_RADIUS;
			nAngleDist = BlueTrendyStaticList.PORTFOLIO_THUMB_DIST;
			nAngleStart = BlueTrendyStaticList.PORTFOLIO_THUMB_ANGLE_START;
			nAngleView = BlueTrendyStaticList.PORTFOLIO_THUMB_ANGLE_VIEW;

			switch(arg_s){
				case BlueTrendyStaticList.MENU_HTML_BUTTON:
				oWindowsVO = oModel.htmlVO;
				break;
				
				case BlueTrendyStaticList.MENU_FLASH_BUTTON:
				oWindowsVO = oModel.flashVO;
				break;
				
				case BlueTrendyStaticList.MENU_MOBILE_BUTTON:
				oWindowsVO = oModel.mobileVO;
				break;
			}
			
			
			iniSignals(true);
			fAddToStage();
		}

		private function fAddToStage() : void {
			mCircle = new CirclePath2D(nRadius, nRadius, nRadius);
//			TweenMax.to(mCircle, 0, {rotation:120, scaleX:1, scaleY:0.8});
			mCircle.rotation = 120;
			for (var i : int = 0; i < oWindowsVO.length; i++) {
				var mWindow : Window = new Window(oWindowsVO[i]);
				var t_nEndAngle : Number = nAngleStart + (nAngleDist * i);
				
//				mContainer.addChildAt(aWindows[aWindows.length-1-i], i);
				mContainer.addChildAt(mWindow, i);
				mWindow.id = i;
				mWindow.angleProgress = t_nEndAngle;
				
				aWindows.push(mWindow);
				aPositions.push(t_nEndAngle);
			}				
				
//			mCircle.visible = false;
			//LU: Hard Coding X,Y Centre Window
			mCircle.y = BlueTrendyStaticList.PORTFOLIO_CIRCLE_Y;
			mCircle.x = BlueTrendyStaticList.PORTFOLIO_CIRCLE_X;
			
//			point = new MovieClip();
//			point.graphics.beginFill(0x000000);
//			point.graphics.drawCircle(0, 0, 5);
//			point.graphics.endFill();
			
//			mContainer.addChild(mCircle);
		}
		
		private function fStartAnimate() : void {
			nThumbLoaded++;
			if(nThumbLoaded == aWindows.length){
//				fCircleFormat();
				fGridFormat();
//				fFirstWin();
			}
			addChild(mContainer);
		}

		private function fGridFormat() : void {
			if(mCurrentWindow != null){
				fShowWindow(false);
				mCurrentWindow.fShowThumb(BlueTrendyStaticList.PORTFOLIO_THUMB_FADEOUT);
				mCurrentWindow.fEnable(false);
				mCurrentWindow = null;
			}
			
			var t_nMaxColumns :Number = 4;
			var t_nXcounter:Number = 0;
			var t_nYcounter:Number = 0;
			var t_nMaxRows :Number = Math.ceil(aWindows.length/t_nMaxColumns);
			
			if(t_nMaxColumns > aWindows.length) t_nMaxColumns = aWindows.length;
			
			bGridFormat = true;
			
			for (var i : int = 0; i < aWindows.length; i++) {
				var t_pSquare :Point = fGetPoint(nAngleView);
				var t_w:Number = ((BlueTrendyStaticList.PORTFOLIO_GRID_DISTANCE + BlueTrendyStaticList.PORTFOLIO_THUMB_WIDTH) *(t_nMaxColumns-1));
				var t_h:Number = ((BlueTrendyStaticList.PORTFOLIO_GRID_DISTANCE + BlueTrendyStaticList.PORTFOLIO_THUMB_HEIGHT + BlueTrendyStaticList.PORTFOLIO_THUMB_INFO_H*t_nMaxRows) *(t_nMaxRows-1));
				var t_x :Number = -t_w/2 +((BlueTrendyStaticList.PORTFOLIO_GRID_DISTANCE + BlueTrendyStaticList.PORTFOLIO_THUMB_WIDTH) *t_nXcounter); 
				var t_y :Number = -t_h/2 +((BlueTrendyStaticList.PORTFOLIO_GRID_DISTANCE + BlueTrendyStaticList.PORTFOLIO_THUMB_HEIGHT  + BlueTrendyStaticList.PORTFOLIO_THUMB_INFO_H) *t_nYcounter);
						
				var t_r:MovieClip = new MovieClip();
				t_r.graphics.beginFill(0x000000);
				t_r.graphics.drawCircle(0, 0, 1);
				t_r.graphics.endFill();
				addChild(t_r);
				
//				t_r.x = t_pSquare.x;
//				t_r.y = t_pSquare.y;
//				t_r.x = 0;
//				t_r.y = 0;
				
				TweenMax.to(aWindows[i], BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {x: t_x, y:t_y, ease: Strong.easeInOut, colorMatrixFilter:{saturation:0}});
				
				if(t_nXcounter+1 < t_nMaxColumns){t_nXcounter ++; 
				}else{ t_nXcounter = 0; t_nYcounter++;}
			}
			
			fSetGrid();
		}
		
		private function fCircleFormat() : void {
			for (var i : int = 0; i < aFrontWin.length; i++) {
				var t_mWindowFront : Window = (aFrontWin[i] as Window);
				var t_nFrontAngle :Number = t_mWindowFront.angleProgress-(nAngleDist*aBackWin.length);
				var t_pWin:Point = fGetPoint(t_nFrontAngle);
				TweenMax.to(aFrontWin[i], BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {x:t_pWin.x, y:t_pWin.y, ease: Strong.easeInOut});
				t_mWindowFront.angleProgress = t_nFrontAngle;
			}
			
			for (var j : int = 0; j < aBackWin.length; j++) {
				var t_mWindowBack : Window = (aBackWin[j] as Window);
				var t_nBackAngle :Number = t_mWindowBack.angleProgress+(nAngleDist*(aFrontWin.length+1));
				var t_pWinBack:Point = fGetPoint(t_nBackAngle);
				TweenMax.to(t_mWindowBack, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {x:t_pWinBack.x, y:t_pWinBack.y, ease: Strong.easeInOut});
				t_mWindowBack.angleProgress = t_nBackAngle;
			}
			
			var t_pCurrWin:Point = fGetPoint(nAngleView);
			
			TweenMax.to(mCurrentWindow, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {x:t_pCurrWin.x, y:t_pCurrWin.y, ease: Strong.easeInOut, onComplete:fShowWindow, onCompleteParams:[true]});
			mCurrentWindow.fShowThumb(BlueTrendyStaticList.PORTFOLIO_THUMB_FADEIN);
			mCurrentWindow.angleProgress = nAngleStart;
			fSetCircle();
			mCurrentWindow.fEnableThumb(false);

			bGridFormat = false;
		}
		
//		private function fFirstWin() : void {
//			bGridFormat = false;
//			for (var i : int = 0; i < aWindows.length; i++) {
//				var t_nBlur :Number = BlueTrendyStaticList.PORTFOLIO_THUMB_BLUR*i;
//				var t_nRotation :Number = BlueTrendyStaticList.PORTFOLIO_THUMB_ROTATE*i;
//				
//				mContainer.addChildAt(aWindows[aWindows.length-1-i], i);
//				TweenMax.from(aWindows[i], .5, {alpha: 0});
//				TweenMax.to(aWindows[i], BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {circlePath2D:{path:mCircle, startAngle:-45, endAngle:aWindows[i].angleProgress, direction:Direction.COUNTER_CLOCKWISE, extraRevolutions:0}, rotation:t_nRotation, blurFilter:{blurX:t_nBlur, blurY:t_nBlur}, colorMatrixFilter:{saturation:0}});
//			}
//			TweenMax.delayedCall(BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, fDefaultChoise);
//		}
		
//		private function fDefaultChoise() : void {
//			mCurrentWindow = aWindows[0];
//			fWindowSelect(aWindows[2]);
////			(mCurrentWindow as Window).angleProgress = nAngleStart;
////			fStartMove();
//		}

		public function fRemoveAll() : void {
//			for (var i : int = 0; i < aWindows.length; i++) {
//				TweenMax.killTweensOf(aWindows[i]);
//				(aWindows[i] as Window).fShowImage(false);
//			}
			if(mCurrentWindow){
				mCurrentWindow.fEnableTimer(false);
				mCurrentWindow.fEnableThumb(false);
				TweenMax.killTweensOf(mCurrentWindow);
			}
			
			iniSignals(false);
		}
		
		public function fRemoveDisplay() :void{
			if(mCurrentWindow) mCurrentWindow.fShowImage(false);
			removeChild(mContainer);
			mContainer = null;
		}
		
		private function fWindowSelect(arg_mc : MovieClip) : void {
			var t_mWindowClicked : Window = (arg_mc as Window);
			if(mCurrentWindow == t_mWindowClicked) return;
			
			trace("LU: Window ID: "+ t_mWindowClicked.id);
			trace("LU: Window ANGLE: "+ t_mWindowClicked.angleProgress);
			
			aFrontWin = [];
			aBackWin = [];
			
			//Hide the last Window
			if(mCurrentWindow != null){
				fShowWindow(false);
				mCurrentWindow.fShowThumb(BlueTrendyStaticList.PORTFOLIO_THUMB_FADEOUT);
			}
			
			for (var i : int = 0; i < aWindows.length; i++) {
				var t_mWindow : Window = (aWindows[i] as Window);
				t_mWindow.fEnable(false);
				if (i == t_mWindowClicked.id) {
					mCurrentWindow = t_mWindow;
				} else {
					if (t_mWindowClicked.angleProgress < t_mWindow.angleProgress) {
						aFrontWin.push(t_mWindow);
					}else{
						aBackWin.push(t_mWindow);
					}	
				}
			}
			
			if(bGridFormat) fCircleFormat(); 
			else fStartMove();
		}
		
		private function fGetPoint(arg_n:Number) :Point{
			var t_p :Point = new Point();
			var t_nAngle :Number = (arg_n +120)*Math.PI/180;
			
			t_p.x = (Math.cos(t_nAngle) *mCircle.radius  + BlueTrendyStaticList.PORTFOLIO_CIRCLE_X);
			t_p.y = (Math.sin(t_nAngle) *mCircle.radius  + BlueTrendyStaticList.PORTFOLIO_CIRCLE_Y);
			
			return t_p;
		}
		
		private function fStartMove() : void {
			trace("fStartMove " + aFrontWin.length);
			for (var i : int = 0; i < aFrontWin.length; i++) {
				var t_mWindowFront : Window = (aFrontWin[i] as Window);
				var t_nFrontAngle :Number = t_mWindowFront.angleProgress-(nAngleDist*aBackWin.length);
//				trace("LU: Front Array Window ID: "+ t_mWindowFront.id +", I: "+ i + ", Window ANGLE: "+ t_mWindowFront.angleProgress + " Window PROGRESS: " + t_nFrontAngle);
				TweenMax.to(t_mWindowFront, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {circlePath2D:{path:mCircle, startAngle:t_mWindowFront.angleProgress, endAngle:t_nFrontAngle, direction:Direction.COUNTER_CLOCKWISE, extraRevolutions:0}});
				t_mWindowFront.angleProgress = t_nFrontAngle;
			}
			
			for (var j : int = 0; j < aBackWin.length; j++) {
				var t_mWindowBack : Window = (aBackWin[j] as Window);
				var t_nBackAngle :Number = t_mWindowBack.angleProgress+(nAngleDist*(aFrontWin.length+1));
				TweenMax.to(t_mWindowBack, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {circlePath2D:{path:mCircle, startAngle:t_mWindowBack.angleProgress, endAngle:t_nBackAngle, direction:Direction.COUNTER_CLOCKWISE, extraRevolutions:0}});
				t_mWindowBack.angleProgress = t_nBackAngle;
			}
			
			//Set the VIEW Position
			TweenMax.to(mCurrentWindow, BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, {circlePath2D:{path:mCircle, startAngle:mCurrentWindow.angleProgress, endAngle:nAngleView, direction:Direction.COUNTER_CLOCKWISE, extraRevolutions:0, colorMatrixFilter:{saturation:0}}, onComplete:fShowWindow, onCompleteParams:[true]});
			mCurrentWindow.fShowThumb(BlueTrendyStaticList.PORTFOLIO_THUMB_FADEIN);
			mCurrentWindow.angleProgress = nAngleStart;
			fSetCircle();
			mCurrentWindow.fEnableThumb(false);
		}
		
		private function fSetGrid() : void {
			for (var i : int = 0; i < aWindows.length; i++) {
				var t_mWindow : Window = (aWindows[i] as Window);
				var t_nBlur :Number = 0;
				var t_nRotation :Number = 0;
				t_mWindow.fSetProperties(t_nRotation, t_nBlur);
				t_mWindow.fShowThumbInfo(true);
				TweenMax.delayedCall(BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, t_mWindow.fEnable, [true]);
//				t_mWindow.fEnable(true);
			}
		}

		private function fSetCircle() : void {
			for (var i : int = 0; i < aWindows.length; i++) {
				var t_mWindow : Window = (aWindows[i] as Window);
				for (var j : int = 0; j < aPositions.length; j++) {
					if(t_mWindow.angleProgress == aPositions[j]){
						var t_nBlur :Number = BlueTrendyStaticList.PORTFOLIO_THUMB_BLUR*j;
						var t_nRotation :Number = BlueTrendyStaticList.PORTFOLIO_THUMB_ROTATE*j;
						mContainer.setChildIndex(t_mWindow, aWindows.length-1-j);
						t_mWindow.fSetProperties(t_nRotation, t_nBlur);
						TweenMax.delayedCall(BlueTrendyStaticList.PORTFOLIO_ROTATE_TIME, t_mWindow.fEnable, [true]);
						if(bGridFormat){
							t_mWindow.fShowThumbInfo(false);
						}
//						t_mWindow.fEnable(true);
					}
				}
			}
		}
		
		private function fShowLoader(arg_b :Boolean) : void {
			if(mCurrentWindow) mCurrentWindow.fShowLoader(arg_b);
		}
		
		private function fImageLoaded(arg_b :Boolean) : void {
			trace("fImageLoaded " + mCurrentWindow);
			if(!mCurrentWindow) return;
			if(arg_b){
				mCurrentWindow.fStartSwap();
			}else{
				mCurrentWindow.fShowThumb(BlueTrendyStaticList.PORTFOLIO_THUMB_HIDE);
			}
		}
		
		private function fShowWindow(arg_b :Boolean) : void {
			if(arg_b){
				mCurrentWindow.fShowNav(true);
				mCurrentWindow.fShowImage(true);
			}else{
				mCurrentWindow.fShowNav(false);
				mCurrentWindow.fShowImage(false);
				mCurrentWindow.fEnableTimer(false);
				mCurrentWindow.fEnableThumb(true);
				TweenMax.killTweensOf(mCurrentWindow);
			}
		}
		
		private function fSwapImage(arg_id: int) : void {
			mCurrentWindow.fSwapImage(arg_id);
		}
	}
}

