package Views.Base {
	import Controller.Controller;

	import Models.Model;

	import flash.display.MovieClip;
	/**
	 * @author LucasOK
	 */
	public class CompositeViewBase extends MovieClip{
		protected var oModel:Model;
		protected var oController:Controller;
		
	public function CompositeViewBase(arg_model:Model = null, arg_controller:Controller=null){
			oModel = arg_model;
			oController = arg_controller;
		}
	}
}
