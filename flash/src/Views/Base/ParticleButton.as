package Views.Base {
	import Utils.Particles.Emitter.MouseDisturb;
	import Utils.Particles.Zones.UniqueBitmapDataZone;

	import com.greensock.*;

	import org.flintparticles.common.events.*;
	import org.flintparticles.twoD.emitters.*;
	import org.flintparticles.twoD.renderers.*;

	import flash.display.*;
	import flash.events.*;
	import flash.geom.*;

    public class ParticleButton extends Sprite {
        private var _enabled:Boolean = true;
        protected var displayBitmap:Bitmap;
        private var _bitmapData:BitmapData;
        private var _renderer:PixelRenderer;
        protected var color:uint;
        protected var hitAreaSprite:Sprite;
        protected var zone:UniqueBitmapDataZone;
        private var _emitter:Emitter2D;
		protected var backgroundBitmap : Bitmap;
		private var _bitmapColor : uint;
//        private static const log:Logger = LogContexxt.getLogger(ParticleButton);

        public function ParticleButton(arg_data:BitmapData = null, arg_color:uint = 0xAAAAAA) {
            _bitmapData = arg_data;
			_bitmapColor = arg_color;
			
            updateUI();
            applyListener();
            updateButtonProperties();
            
        }
		
        public function get bitmapData() : BitmapData {
            return _bitmapData;
        }
        public function get enabled() : Boolean {
            return _enabled;
        }
        private function applyListener() : void {
            addEventListener(MouseEvent.ROLL_OVER, handleRollOver);
//            addEventListener(MouseEvent.ROLL_OUT, handleRollOut);
		}

		private function handleRollOut(event :MouseEvent) : void {
			disableEmitter();
		}
		
//        private function stopEmitter() : void {
        private function stopEmitter(event:EmitterEvent) : void {
			trace("stopEmitter");
            _emitter.stop();
            _emitter = null;
            removeChild(_renderer as DisplayObject);
            _renderer = null;
            
        }
        public function set enabled(param1:Boolean) : void {
            _enabled = param1;
            updateButtonProperties();
            
        }
        protected function updateUI() : void {
            if (bitmapData == null){
                return;
            }
            cleanUpUi();
            zone = new UniqueBitmapDataZone(bitmapData);
            displayBitmap = new Bitmap(bitmapData, "auto", true);
            backgroundBitmap = new Bitmap(bitmapData);
            TweenMax.to(backgroundBitmap, 0, {tint:_bitmapColor});
            addChild(backgroundBitmap);
            addChild(displayBitmap);
			
			drawHitarea();
        }
		
        protected function disableEmitter() : void {
            if (_emitter == null){
                return;
            }
//			stopEmitter();
            _emitter.addEventListener(EmitterEvent.EMITTER_UPDATED, stopEmitter);
            
        }
        protected function enableEmitter() : void {
            var _loc_1:int = 0;
            var _loc_2:int = 0;
            if (_renderer == null){
                _loc_1 = width + 30;
                _loc_2 = height + 30;
                _renderer = new PixelRenderer(new Rectangle(-15, -15, _loc_1, _loc_2));
                addChild(_renderer as DisplayObject);
            }
            if (_emitter == null){
                _emitter = new MouseDisturb(zone, _renderer);
                _emitter.addEventListener(MouseDisturb.COMPLETE, handleMouseDisturbComplete, false, 0, true);
                _renderer.addEmitter(_emitter);
                _emitter.start();
            }
            
        }
        private function updateButtonProperties() : void {
            buttonMode = true;
            useHandCursor = enabled;
        }
		
        public function set bitmapData(param1:BitmapData) : void {
            _bitmapData = param1;
            updateUI();
            
        }
        private function cleanUpUi() : void {
            disableEmitter();
            if (backgroundBitmap != null && contains(backgroundBitmap)){
                removeChild(backgroundBitmap);
            }
            if (displayBitmap != null && contains(displayBitmap)){
                removeChild(displayBitmap);
            }
            
        }
        private function handleRollOver(event:MouseEvent) : void {
            if (enabled){
                TweenMax.to(displayBitmap, 0.2, {autoAlpha:0});
                enableEmitter();
            }
            
        }
        private function handleMouseDisturbComplete(event:Event) : void {
            TweenMax.to(displayBitmap, 0, {autoAlpha:1});
            disableEmitter();
            
        }
        private function drawHitarea() : void {
            graphics.clear();
            graphics.beginFill(16711680, 0);
            graphics.drawRect(0, 0, width, height);
            
        }
    }
}