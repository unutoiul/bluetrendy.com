package Views.Contact.Form {
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Utils.ApplicationFonts;
	import Utils.Buttons;

	import Views.Base.CompositeViewBase;
	import Views.Contact.Form.ui.FormStaticVars;
	import Views.Contact.Form.ui.NormalFormField;

	import com.greensock.TweenMax;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.MouseEvent;
	import flash.net.URLLoader;
	import flash.net.URLRequest;
	import flash.net.URLRequestHeader;
	import flash.net.URLRequestMethod;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Form extends CompositeViewBase {
		private var tName 				: NormalFormField;
		private var tTelephone 			: NormalFormField;
		private var tEmail 				: NormalFormField;
		private var tMessage 			: NormalFormField;
		private var tTitle 				: TextField;
		private var aFields 			: Array;
		private var mSubmitBT 			: MovieClip;
		private var nChar 				: int;
		private var bChar : Boolean;
		private var tInfo : TextField;
		public function Form(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			
			ini();
		}

		private function ini() : void {
			var t_fTitle :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_TITLE_FONT_SIZE, 0x111111);
//			TweenMax.to(tTitle as MovieClip, 0., {dropShadowFilter:{color:0x000000, alpha:1, blurX:4, blurY:4, strength:1, angle:90, distance:1}});
			tTitle = ApplicationFonts.fCreateText(t_fTitle);
			tTitle.text = BlueTrendyStaticList.CONTACT_TITLE;
			
			tName = new NormalFormField("Full Name", 16, FormStaticVars.FIELD_WIDTH, 0x333333, 0xFFFFFF);
			tTelephone = new NormalFormField("Telephone", 16, FormStaticVars.FIELD_WIDTH, 0x333333, 0xFFFFFF);
			tEmail = new NormalFormField("Email Address", 16, FormStaticVars.FIELD_WIDTH, 0x333333, 0xFFFFFF);
			tMessage = new NormalFormField("Message", 16, FormStaticVars.FIELD_WIDTH, 0x333333, 0xFFFFFF);
			mSubmitBT = Buttons.fCreateNormalButton(100, 25, 25, 0x333333, "SUBMIT");
			
			aFields = new Array(tTitle, tName, tTelephone, tEmail, tMessage, mSubmitBT);
			
			tMessage.getInputText.wordWrap = true;
			tMessage.getInputText.multiline = true;
			
			nChar = 0;
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			var t_nPositon :Number; 
			
			tMessage.getInputText.height = tMessage.getInputText.height*5;
			tMessage.getInputShape.height = tMessage.getInputText.height + FormStaticVars.FIELD_INPUT_SHAPE_MARGIN*2;
			
			tTitle.selectable = true;
			
			fShowFields(true);
			
			fSubmitEnabled(true);
		}

		private function fShowFields(arg_b : Boolean) : void {
			if(arg_b){
				for (var i : int = 0; i < aFields.length; i++) {
					if(i==0) aFields[i].y = i; else aFields[i].y = aFields[i-1].y + aFields[i-1].height + FormStaticVars.FIELD_VERTICAL_SPACE;
					
					if(aFields[i] is TextField){
						aFields[i].x = aFields[aFields.length-2].getInputShape.x;
					}else if(aFields[i].getInputText){
						aFields[i].getInputText.tabIndex = i;
					}else if(!(aFields[i] is TextField)){
						aFields[i].tabIndex = i;
						aFields[i].x = aFields[aFields.length-2].getInputShape.x;
					}
					addChild(aFields[i]);
					
				}
			}else{
				for (var j : int = 0; j < aFields.length; j++) {
					TweenMax.to(aFields[j], 0.5, {autoAlpha:0});
//					removeChild(aFields[j]);
				}
			}
		}

		private function fSubmitEnabled(arg_b : Boolean) : void {
			if(arg_b){
				mSubmitBT.addEventListener(MouseEvent.MOUSE_DOWN, fSubmitForm); 
			}else{
				mSubmitBT.removeEventListener(MouseEvent.MOUSE_DOWN, fSubmitForm); 
			}
		}

		private function fSubmitForm(event: MouseEvent) : void {
			fCheckField(tName, false);
			fCheckField(tTelephone, false);
			fCheckField(tEmail, true);
			fCheckField(tMessage, false);
			
			if(!tName.bIsEmpty && !tTelephone.bIsEmpty && !tEmail.bIsEmpty && tEmail.bIsValidEmail && !tMessage.bIsEmpty){
				fSubmitEnabled(false);
				fSendingScreen(true);
//				fSendEmail();	
			}
		}

		private function fSendingScreen(arg_b : Boolean) : void {
			fShowFields(false);
			fCreateMessage();
			fUpdatewMessage(BlueTrendyStaticList.CONTACT_TITLE_SENDING, BlueTrendyStaticList.CONTACT_SENDING_MESSAGE);
			
			fAniMessage();
			fSendEmail();
		}

		private function fAniMessage() : void {
			if(nChar !=0) tTitle.text = BlueTrendyStaticList.CONTACT_TITLE_SENDING.slice(0, -nChar); else tTitle.text = BlueTrendyStaticList.CONTACT_TITLE_SENDING;
			if(nChar == 3) bChar = true; else if(nChar == 0) bChar = false;
			if(bChar) nChar--; else nChar++;
			
			TweenMax.delayedCall(.3, fAniMessage);
		}
		
		public function fSendEmail() : void {
			var t_sUrl :String = BlueTrendyStaticList.MAIL_GATEWAY + 
			"?secret=" + BlueTrendyStaticList.SECRET_KEY + 
			"&name=" + tName.getInputText.text + 
			"&telephone=" + tTelephone.getInputText.text + 
			"&email=" +  tEmail.getInputText.text +
			"&message=" +  tMessage.getInputText.text;
			
			var varLoad:URLLoader = new URLLoader();
			var urlRequest:URLRequest = new URLRequest(t_sUrl);
			var header:URLRequestHeader = new URLRequestHeader("Content-type","application/octet-stream");
			urlRequest.requestHeaders.push(header);
			urlRequest.method = URLRequestMethod.POST;
			
			trace("t_sUrl " + t_sUrl);
			
			try{	
				varLoad.load(urlRequest);
				varLoad.addEventListener(Event.COMPLETE, fCheckSend);
				varLoad.addEventListener(IOErrorEvent.IO_ERROR, fIOError);
				trace("Sending email...please wait...");
			}catch(error:Error){
				trace("LucasOK: Error - cannot load the URL Request");
			}
		}
	
		private function fCheckSend(event:Event):void {
			var t_sSent :String = "yes"; 
			var t_sMessage :String; 
			
			TweenMax.killDelayedCallsTo(fAniMessage);
			
			t_sSent = fStripspaces(t_sSent);
			trace("LucasOK: Sent " + t_sSent);
			
			if(t_sSent == "yes") {
				t_sMessage = BlueTrendyStaticList.CONTACT_THANKS_MESSAGE;
			} else {
				t_sMessage = BlueTrendyStaticList.CONTACT_ERROR_MESSAGE;
			}
			
			fUpdatewMessage(BlueTrendyStaticList.CONTACT_TITLE_THANKS, t_sMessage);
		}

		private function fUpdatewMessage(arg_title : String, arg_message:String) : void {
			tTitle.text = arg_title;
			tInfo.text = arg_message;
		}
		
		private function fCreateMessage() : void {
			var t_fMessage :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_BUTTON_FONT_SIZE, BlueTrendyStaticList.COLOR_GREY_DARK);
			tInfo = ApplicationFonts.fCreateText(t_fMessage);
			
			tInfo.selectable = true;
			tInfo.wordWrap = true;
			tInfo.multiline = true;
			
			tInfo.width = BlueTrendyStaticList.PAGE_LINE_WIDTH - tTitle.x;
			
			tInfo.y = tTitle.height;
			tInfo.x = tTitle.x;
//			tTitle.x = tTitle.y = 0;
			
			TweenMax.to(tTitle, .5, {autoAlpha:1});
			TweenMax.from(tInfo, .5, {autoAlpha:0});
			
			addChild(tTitle);
			addChild(tInfo);
		}
		
		private function fStripspaces(arg_s:String):String {
			var original:Array=arg_s.split(" ");
			return(original.join(""));
		}
		
		private function fIOError(event : IOErrorEvent) : void {
			trace("fIOError " + event.text);
		}
		
		private function fCheckField(arg_field:NormalFormField, arg_email_valid :Boolean) :void{
			if(arg_email_valid){
				if(!arg_field.bIsValidEmail)fFieldError(arg_field.getInputShape, true); else
					fFieldError(arg_field.getInputShape, false);
			}else{
				if(arg_field.bIsEmpty) fFieldError(arg_field.getInputShape, true);
					else fFieldError(arg_field.getInputShape, false);
			}
		}
		
		private function fFieldError(arg_shape:MovieClip, arg_b :Boolean) :void{
			if(arg_b){
				TweenMax.to(arg_shape, .5, {alpha:.5, colorTransform:{tint:BlueTrendyStaticList.COLOR_RED, tintAmount:1}});
			}else{
				TweenMax.to(arg_shape, .5, {alpha:.5, colorTransform:{tint:BlueTrendyStaticList.COLOR_RED, tintAmount:0}});
			}
		}
	}
}
