package Views.Contact.Form.ui {
	import com.greensock.TweenLite;
	import Utils.ApplicationFonts;

	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.FocusEvent;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class NormalFormField extends MovieClip {
		private var mInputShape				: MovieClip;
		private var tInputText 				: TextField;
		private var tNameText 				: TextField;
			
//		public var SET_FOCUS_FIELD : Signal = new Signal(TextField);
		
		public function NormalFormField(arg_name:String, arg_size:Number, arg_width :Number, arg_txt_color:uint =0x000000, arg_input_color:uint = 0xFFFFFF) :void{
		//constructor
			var t_fNameFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, arg_size, arg_txt_color, "right");
			var t_fInputFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, arg_size, arg_input_color);
			
			mInputShape = new MovieClip();
			tInputText = ApplicationFonts.fCreateInputText(t_fInputFormat);
			tNameText = ApplicationFonts.fCreateDynamicText(t_fNameFormat);
			
			tNameText.wordWrap = false;
			tNameText.multiline = false;
			tNameText.text = arg_name;
			
			tNameText.x = -tNameText.width +72;
			
			tInputText.width = arg_width;
			tInputText.height = arg_size + 5;
//			tInputText.name = arg_name;
			
			tInputText.x = FormStaticVars.FIELD_INPUT_X;
			
			mInputShape.graphics.beginFill(FormStaticVars.FIELD_INPUT_SHAPE_COLOR);
			mInputShape.graphics.drawRect(0, 0, tInputText.width+FormStaticVars.FIELD_INPUT_SHAPE_MARGIN*2, tInputText.height+FormStaticVars.FIELD_INPUT_SHAPE_MARGIN*2);
//			mInputShape.graphics.drawRoundRect(0, 0, tInputText.width+FormStaticVars.FIELD_INPUT_SHAPE_MARGIN*2, tInputText.height+FormStaticVars.FIELD_INPUT_SHAPE_MARGIN*2, 10, 10);
			mInputShape.graphics.endFill();
			
			mInputShape.alpha = .5;
			
			mInputShape.x = tInputText.x -FormStaticVars.FIELD_INPUT_SHAPE_MARGIN;
			mInputShape.y = -FormStaticVars.FIELD_INPUT_SHAPE_MARGIN;
			
			tNameText.mouseEnabled = false;
//			tInputText.mouseEnabled = false;
			
			doubleClickEnabled = true;
				
			addEventListener(MouseEvent.MOUSE_DOWN, fSetFocus);
			addEventListener(MouseEvent.DOUBLE_CLICK, fSetAll);
//			
			tInputText.addEventListener(FocusEvent.FOCUS_OUT, fSetFocus);
			tInputText.addEventListener(FocusEvent.FOCUS_IN, fSetFocus);
			
			addChild(mInputShape);
			addChild(tInputText);
			
			addChild(tNameText);
		}

		private function fSetAll(event : MouseEvent) : void {
			tInputText.setSelection(0, tInputText.text.length);
		}

		private function fSetFocus(event : Event) : void {
			trace("event.type " + event.type);
			
			switch(event.type){
				case "focusOut":
//				mInputShape.alpha = .5;
				TweenLite.to(mInputShape, .4, {alpha:.5});
				break;
				
				case "focusIn":
				TweenLite.to(mInputShape, .4, {alpha:1});
//				mInputShape.alpha = 1;
				break;
			}
//			tInputText.setSelection(tInputText.text.length, tInputText.text.length);

//			FormEvents.SET_FOCUS_FIELD.dispatch(mInput);
		}
		
		
		public function get bIsEmpty():Boolean {
			if(tInputText.text == ""){
				return true;
			}else{
				return false;
			}	
		}
		
		public function get bIsValidEmail():Boolean {
			var regExpPattern : RegExp = /^[0-9a-zA-Z][-._a-zA-Z0-9]*@([0-9a-zA-Z][-._0-9a-zA-Z]*\.)+[a-zA-Z]{2,4}$/;
			if(tInputText.text.match(regExpPattern) == null ) {  
				return false;
			} else {
				return true;
			}
		}

		public function get getInputShape() : MovieClip {
			return mInputShape;
		}

		public function get getInputText() : TextField {
			return tInputText;
		}
	}
}
