package Views.Contact.Form.ui {
	import Utils.ApplicationFonts;

	import flash.display.MovieClip;
	import flash.display.Sprite;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class CheckBox extends MovieClip {
		private static const CHECKBOX_BORDER_SIZE 	: Number = 1;
		private static const CHECKBOX_CROSS_BORDER 	: Number = 5;
		private static const CHECKBOX_CROSS_SPACE 	: Number = 7;
		private static const CHECKBOX_SPACE 		: Number = 10;
		
		private var tNameText 						: TextField;
		
		private var mCross 							: Sprite;
		private var mBox 							: MovieClip;
		private var bBoxChecked 					: Boolean;
		
		public function CheckBox(arg_name :String, arg_size:Number, arg_link :String, arg_txtColor:uint) :void{
			var t_fNameFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, arg_size, arg_txtColor);
			
			tNameText = ApplicationFonts.fCreateDynamicText(t_fNameFormat);
			tNameText.text = arg_name;
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			mBox = new MovieClip();
			mBox.graphics.beginFill(0x000000);
			mBox.graphics.lineStyle(CHECKBOX_BORDER_SIZE, 0xFFFFFF);
			mBox.graphics.drawRect(0, 0, tNameText.height, tNameText.height);
			mBox.graphics.endFill();
			
			mCross = new Sprite();
			mCross.graphics.lineStyle(CHECKBOX_CROSS_BORDER , 0xffffff);
			mCross.graphics.moveTo(-tNameText.height/2+CHECKBOX_CROSS_SPACE , -tNameText.height/2+CHECKBOX_CROSS_SPACE);
			mCross.graphics.lineTo(tNameText.height/2-CHECKBOX_CROSS_SPACE , tNameText.height/2-CHECKBOX_CROSS_SPACE);
			mCross.graphics.moveTo(-tNameText.height/2+CHECKBOX_CROSS_SPACE , tNameText.height/2-CHECKBOX_CROSS_SPACE);
			mCross.graphics.lineTo(tNameText.height/2-CHECKBOX_CROSS_SPACE, -tNameText.height/2+CHECKBOX_CROSS_SPACE);
			
			tNameText.x = mBox.width + CHECKBOX_SPACE;
			tNameText.y = mBox.height/2 - tNameText.height/2;
			
			mCross.x = mCross.width/2 + CHECKBOX_CROSS_SPACE/2 + CHECKBOX_BORDER_SIZE;
			mCross.y = mCross.height/2 + CHECKBOX_CROSS_SPACE/2 + CHECKBOX_BORDER_SIZE;
			
			mCross.visible = false;
			bBoxChecked = false;
			
			mBox.addEventListener(MouseEvent.MOUSE_DOWN, fCheckBox);
			
			mBox.addChild(mCross);
			
			addChild(tNameText);
			addChild(mBox);
		}

		private function fCheckBox(event : MouseEvent) : void {
			bBoxChecked = !bBoxChecked;
			mCross.visible = !mCross.visible;
			
			trace("bBoxChecked " + bBoxChecked);
		}
		
		public function get bIfBoxChecked() : Boolean {
			return bBoxChecked;
		}
	}
}
