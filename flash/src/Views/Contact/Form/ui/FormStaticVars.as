package Views.Contact.Form.ui {
	/**
	 * @author LucasOK
	 */
	public class FormStaticVars {
		public static var FIELD_WIDTH									:Number	= 300;
		public static var FIELD_VERTICAL_SPACE							:Number	= 20;
//		public static var FIELD_INPUT_SPACE_V							:Number	= 40;
		public static var FIELD_INPUT_X									:Number	= 100;
		public static var FIELD_INPUT_SHAPE_COLOR						:uint	= 0x424242;
		public static var FIELD_INPUT_SHAPE_MARGIN						:Number	= 10;
	}
}
