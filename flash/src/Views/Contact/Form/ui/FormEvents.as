package Views.Contact.Form.ui {
	import flash.display.MovieClip;
	import org.osflash.signals.Signal;

	import flash.text.TextField;
	/**
	 * @author LucasOK
	 */
	public class FormEvents {
//		public static const SET_FOCUS_FIELD					:Signal	= new Signal(MovieClip);
//		public static const SET_FOCUS_FORM					:Signal	= new Signal(TextField);
//		public static const SET_DYNAMIC_TEXT				:Signal	= new Signal(TextField);
		public static const SEND_FORM						:Signal	= new Signal();
		public static const SEND_FORM_SUCCESS				:Signal	= new Signal();
	}
}
