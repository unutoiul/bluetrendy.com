package Views.Contact {
	import Controller.Controller;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;
	import Models.VO.OfficeVO;

	import Utils.ApplicationFonts;

	import Views.Base.CompositeViewBase;

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Offices extends CompositeViewBase {
		private var tTitle : TextField;
		public function Offices(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			
			ini();
		}

		private function ini() : void {
			var t_fTitle :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_TITLE_FONT_SIZE, 0x111111);
			tTitle = ApplicationFonts.fCreateText(t_fTitle);
			
			tTitle.text = "OUR OFFICES";
			tTitle.selectable = true;
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			for (var i : int = 0; i < oModel.officesVO.length; i++) {
				var t_mOffice :MovieClip = fCreateOffice(oModel.officesVO[i]);
				
				if(i==0) t_mOffice.y = tTitle.height; else t_mOffice.y = t_mOffice.y + tTitle.height + t_mOffice.height*i;
				
				addChild(t_mOffice);
			}
			
			addChild(tTitle);
		}
		
		private function fCreateOffice(arg_vo :OfficeVO) : MovieClip {
			var t_mOffice:MovieClip = new MovieClip();
			var t_fTitle :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, 18,BlueTrendyStaticList.COLOR_YELLOW);
			var t_fBody :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, 14, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			
			var t_tTitle :TextField = ApplicationFonts.fCreateText(t_fTitle);
			var t_tBody :TextField = ApplicationFonts.fCreateText(t_fBody);
			
			t_tTitle.selectable = true;
			t_tBody.selectable = true;
			
			t_tTitle.text = arg_vo.location;
//			t_tBody.text = arg_vo.address + "\n"+ arg_vo.person + "\nE." + arg_vo.email + "\nT. " + arg_vo.telephone;
			t_tBody.text = arg_vo.address + "\nE. " + arg_vo.email + "\nT. " + arg_vo.telephone;
			
			t_tBody.y = t_tTitle.height;

			t_mOffice.addChild(t_tTitle);
			t_mOffice.addChild(t_tBody);
			
			return t_mOffice;
		}
	}
}
