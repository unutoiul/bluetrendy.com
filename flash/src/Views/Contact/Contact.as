package Views.Contact {
	import Controller.Controller;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Views.Base.CompositeViewBase;
	import Views.Contact.Form.Form;
	/**
	 * @author LucasOK
	 */
	public class Contact extends CompositeViewBase {
		private var mForm 				: Form;
		private var mOffices 			: Offices;
		
		public function Contact(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			ini();
		}

		private function ini() : void {
			mOffices = new Offices(oModel, oController);
			mForm = new Form(oModel, oController);
		
			fAddToStage();
		}

		private function fAddToStage() : void {
			mOffices.x = BlueTrendyStaticList.PAGE_COLUMN_WIDTH;
			trace("mForm " + mForm.x);
			addChild(mForm);
			addChild(mOffices);
		}
	}
}
