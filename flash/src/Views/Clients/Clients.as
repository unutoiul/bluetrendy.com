package Views.Clients {
	import Controller.Controller;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;
	import Models.VO.ClientsVO;

	import Utils.ApplicationFonts;

	import Views.Base.CompositeViewBase;

	import com.greensock.TweenMax;

	import flash.display.Bitmap;
	import flash.display.Loader;
	import flash.display.MovieClip;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.net.URLRequest;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Clients extends CompositeViewBase {
		private var mSecoundLine 		: MovieClip;
		private var mSubTitle 			: MovieClip;
		private var mFirstLine 			: MovieClip;
		private var mLogos 				: MovieClip;
		
		private var fTitle 				: TextFormat;
		private var fSubTitle 			: TextFormat;
		private var fBody 				: TextFormat;
		
		private var tTitle 				: TextField;
		private var tSubTitle 			: TextField;
		private var tBody 				: TextField;
		
		private var oClientsVO 			: ClientsVO;
		private var lLoader 			: Loader;
		private var nLogoLoaded 		: int;
		private var aLogosMC 			: Array;

		public function Clients(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			ini();
		}

		private function ini() : void {
			fTitle = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_TITLE_FONT_SIZE, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			fSubTitle = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_SUBTITLE_FONT_SIZE, BlueTrendyStaticList.COLOR_YELLOW, "right");
			fBody = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, BlueTrendyStaticList.PAGE_BODY_FONT_SIZE, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			
			tTitle = ApplicationFonts.fCreateText(fTitle);
			tSubTitle = ApplicationFonts.fCreateText(fSubTitle);
			tBody = ApplicationFonts.fCreateText(fBody);
			
			tTitle.selectable = tSubTitle.selectable = tBody.selectable = true;
			
			mFirstLine = fCreateLine(1);
			mSecoundLine = fCreateLine(5);
			mSubTitle = fCreateSlogan();
			mLogos = new MovieClip();
			aLogosMC = new Array();
			
			oClientsVO = oModel.clientsVO;
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			tTitle.text = oClientsVO.title;
			tSubTitle.text = oClientsVO.subtitle;
			tBody.htmlText = oClientsVO.body;
			
			mFirstLine.y = tTitle.height;
			
			mSecoundLine.y = 250;
			
			tTitle.x = BlueTrendyStaticList.PAGE_COLUMN_WIDTH;
			
			tSubTitle.multiline = true;
			tSubTitle.wordWrap = true;
			tSubTitle.width = 320;
			
			mSubTitle.x = BlueTrendyStaticList.PAGE_LINE_WIDTH - tSubTitle.width;
			mSubTitle.y =  mSecoundLine.y/2 - tSubTitle.height/2 + mFirstLine.y/2;
			
			tBody.multiline = true;
			tBody.wordWrap = true;
			tBody.width = BlueTrendyStaticList.PAGE_LINE_WIDTH;
			tBody.x = BlueTrendyStaticList.PAGE_COLUMN_WIDTH;
			tBody.y = tTitle.height;
			
			mLogos.y = tTitle.height;
			
			fCreateLogos();
			
			addChild(mFirstLine);
			addChild(mSecoundLine);
			addChild(tTitle);
//			addChild(mSubTitle);
			addChild(tBody);
			addChild(mLogos);
		}

		private function fCreateSlogan() : MovieClip {
			var t_mSubTitle :MovieClip = new MovieClip();
			t_mSubTitle.addChild(tSubTitle);
			TweenMax.to(t_mSubTitle, 0., {dropShadowFilter:{color:BlueTrendyStaticList.COLOR_BLACK_LIGHT, alpha:1, blurX:2, blurY:2, strength:1, distance:3}});
			return t_mSubTitle;
		}
		
		private function fCreateLine(arg_h:Number) : MovieClip {
			var t_sLine :MovieClip = new MovieClip();
			t_sLine.graphics.beginFill(BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			t_sLine.graphics.drawRect(0, 0, BlueTrendyStaticList.PAGE_LINE_WIDTH, arg_h);
			t_sLine.graphics.endFill();
			return t_sLine;
		}
		
		private function fCreateLogos() : void {
			for (var i : int = 0; i < oClientsVO.logos.length; i++) {
				lLoader = new Loader();
				lLoader.load(new URLRequest(oClientsVO.logos[i]));
				lLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, fLoadLogo);
				lLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, fLoadError);
			}
		}

		private function fLoadError(event :IOErrorEvent) : void {
			trace(event.text);
		}

		private function fLoadLogo(event : Event) : void {
			var t_mLogo :Bitmap = Bitmap(event.target.content);
			t_mLogo.smoothing = true;
			
			aLogosMC.push(t_mLogo);
			nLogoLoaded ++;
			if(nLogoLoaded == oClientsVO.logos.length-1) fShowLogos();
		}

		private function fShowLogos() : void {
			var t_nMaxColumns :Number = 5;
			var x_counter:Number = 0;
			var y_counter:Number = 0;
			
			for (var i : int = 0; i < aLogosMC.length; i++) {
				var t_mSquare :MovieClip = new MovieClip();
				t_mSquare.graphics.beginFill(0xff0033, 0);
				t_mSquare.graphics.drawRect(0, 0, 60, 60);
				t_mSquare.graphics.endFill();
				
				TweenMax.from(aLogosMC[i], .5,{alpha:0, delay:.4*i*0.1});
				
				t_mSquare.addChild(aLogosMC[i]);
				
				aLogosMC[i].x = t_mSquare.width/2 - aLogosMC[i].width/2;
				aLogosMC[i].y = t_mSquare.height/2 - aLogosMC[i].height/2;
				
				t_mSquare.x = 90 *x_counter;
				t_mSquare.y = (9 + t_mSquare.height) *y_counter;
				
				if(x_counter+1 < t_nMaxColumns){ x_counter ++; 
					}else{ x_counter = 0; y_counter++;}
				
				mLogos.addChild(t_mSquare);
			}
		}
	}
}
