package Views {
	import Lookups.BlueTrendyDynamicList;
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Views.Base.CompositeViewBase;
	import Views.Clients.Clients;
	import Views.Contact.Contact;
	import Views.Home.Home;
	import Views.Portfolio.Portfolio;
	import Views.Solutions.Solutions;

	import com.asual.swfaddress.SWFAddress;
	import com.asual.swfaddress.SWFAddressEvent;
	import com.greensock.TweenMax;
	import com.greensock.easing.Sine;
	import com.greensock.plugins.TransformAroundCenterPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.MovieClip;
	
	/**
	 * @author LucasOK
	 */
	
	public class Pages extends CompositeViewBase{
		private var mCurrentPage 			: MovieClip;
		private var mLastPage 				: MovieClip;
		
		public function Pages(arg_model: Model, arg_controller: Controller):void {
			super(arg_model, arg_controller);
			ini();
		}

		private function ini() : void {
			fAddToStage();
		}

		private function fAddToStage() : void {
//			mCurrentPage = new Home(oModel, oController);
			
			SWFAddress.addEventListener(SWFAddressEvent.CHANGE, fHandleSWFAddress);
			CustomSignals.BUTTONS_ENABLED.dispatch(true);
			
//			fPositionPage();
//			fShowPage();
		}

		private function fHandleSWFAddress(event :SWFAddressEvent) : void {
			try{
				var t_sAddress :String = event.path.substr(1).toUpperCase();
				if(mCurrentPage == null){
					if(t_sAddress == "") fChangePage(BlueTrendyStaticList.MENU_HOME_BUTTON);
						else fChangePage(t_sAddress);
				}
				
				trace("fHandleSWFAddress " + event.path);
			}catch(error:Error){
				trace("SWF Object address is not found!");	
			}
					
			var t_sTitle :String='BlueTrendy LTD';
			
			for (var i :int= 0; i < event.pathNames.length; i++) {
				t_sTitle+=' > '+event.pathNames[i].substr(0,1).toUpperCase()+event.pathNames[i].substr(1);
			}

			SWFAddress.setTitle(t_sTitle);
		}
		
		public function fChangePage(arg_s :String):void{
			trace("fChangePage " + arg_s);
			if(mCurrentPage){
				mLastPage = null;
				mLastPage = mCurrentPage;
				mLastPage.alpha = 1;
				mCurrentPage = null;

				CustomSignals.BUTTONS_ENABLED.dispatch(false);

				fHidePage();
			}
			
			switch(arg_s){
				case BlueTrendyStaticList.MENU_HOME_BUTTON:
				mCurrentPage = new Home(oModel, oController);
				break;		
				
				case BlueTrendyStaticList.FOOTER_CLIENTS_BUTTON:
				mCurrentPage = new Clients(oModel, oController);
				break;		
					
				case BlueTrendyStaticList.FOOTER_SOLUTIONS_BUTTON:
				mCurrentPage = new Solutions(oModel, oController);;
				break;
				
				case BlueTrendyStaticList.FOOTER_CONTACT_BUTTON:
				mCurrentPage = new Contact(oModel, oController);
				break;
				
				case BlueTrendyStaticList.MENU_FLASH_BUTTON:
				mCurrentPage = new Portfolio(oModel, oController);
				mCurrentPage.ini(BlueTrendyStaticList.MENU_FLASH_BUTTON);
				break;
				
				case BlueTrendyStaticList.MENU_HTML_BUTTON:
				mCurrentPage = new Portfolio(oModel, oController);
				mCurrentPage.ini(BlueTrendyStaticList.MENU_HTML_BUTTON);
				break;
				
				case BlueTrendyStaticList.MENU_MOBILE_BUTTON:
				mCurrentPage = new Portfolio(oModel, oController);
				mCurrentPage.ini(BlueTrendyStaticList.MENU_MOBILE_BUTTON);
				break;
			}
			
			mCurrentPage.alpha = 1;
			mCurrentPage.rotationY = 0;
			mCurrentPage.scaleX = mCurrentPage.scaleY = 1;
			mCurrentPage.x = mCurrentPage.y = 0;

			addChild(mCurrentPage);
			
			BlueTrendyDynamicList.CURRENT_PAGE = arg_s;
			SWFAddress.setValue(arg_s.toLowerCase());

			fPositionPage();
			fShowPage();
		}
		 
		private function fShowPage():void{
			if(mCurrentPage is Portfolio){
				TweenMax.from(mCurrentPage, 1, {y:BlueTrendyStaticList.APP_HEIGHT, scaleY: .7, scaleX: .7, rotationY:-30, ease:Sine.easeOut, delay:.5});
				TweenMax.from(mCurrentPage, .2, {alpha:0, delay:.6});
			}else{
				TweenMax.from(mCurrentPage, 1, {x:BlueTrendyStaticList.APP_WIDTH, scaleY: .7, scaleX: .7, rotationY:-30, ease:Sine.easeOut, delay:.5});
				TweenMax.from(mCurrentPage, .2, {alpha:0, delay:.6});
			}
		}
//		
		private function fHidePage():void{
			if(mLastPage is Portfolio){
				(mLastPage as Portfolio).fRemoveAll();
				TweenMax.to(mLastPage, 1, {transformAroundCenter:{y: 0, scaleY: .7, scaleX: .7, rotationY:30}, ease:Sine.easeIn});
				TweenMax.to(mLastPage, .2, {alpha:0, delay: .6, onComplete:fRemove});
			}else{
				TweenMax.to(mLastPage, 1, {transformAroundCenter:{x: 0, scaleY: .7, scaleX: .7, rotationY:30}, ease:Sine.easeIn});
				TweenMax.to(mLastPage, .2, {alpha:0, delay: .6, onComplete:fRemove});
			}	
		}
		
		private function fRemove():void{
			if(mLastPage is Portfolio){
				(mLastPage as Portfolio).fRemoveDisplay();
			}
			CustomSignals.BUTTONS_ENABLED.dispatch(true);
			removeChild(mLastPage);
		}

		public function fPositionPage() : void {
//			trace("fPositionPage W: "+ mCurrentPage.width, " H: ", mCurrentPage.height);
			if(mCurrentPage is Portfolio){
				mCurrentPage.y = BlueTrendyStaticList.APP_HEIGHT/2;
				mCurrentPage.x = BlueTrendyStaticList.APP_WIDTH/2;
			}else if(mCurrentPage){
				mCurrentPage.y = BlueTrendyStaticList.APP_HEIGHT/2 - mCurrentPage.height /2;
				mCurrentPage.x = BlueTrendyStaticList.APP_WIDTH/2 - mCurrentPage.width /2;
			}	
		}
	}
}
