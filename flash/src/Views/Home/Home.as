package Views.Home {
	import Controller.Controller;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Utils.ApplicationFonts;

	import Views.Base.CompositeViewBase;

	import com.greensock.plugins.TransformAroundCenterPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.MovieClip;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Home extends CompositeViewBase {
		TweenPlugin.activate([TransformAroundCenterPlugin]);
		private var fTitle : TextFormat;
		private var tTitle : TextField;
		private var mQuotation : MovieClip;

		public function Home(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			ini();
		}

		private function ini() : void {
			fTitle = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, BlueTrendyStaticList.PAGE_TITLE_FONT_SIZE, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			mQuotation = new quotationMark();

			tTitle = ApplicationFonts.fCreateText(fTitle);
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			tTitle.text = oModel.homeTitle;
			
			tTitle.multiline = true;
			tTitle.wordWrap = true;
//			tTitle.border = true;
			
			tTitle.width = 700;
			
			tTitle.x = mQuotation.width;
			tTitle.y = mQuotation.height;
			
			this.x = BlueTrendyStaticList.APP_WIDTH/2 - this.width/2;
			this.y = BlueTrendyStaticList.APP_HEIGHT/2 - this.height/2;

			addChild(tTitle);
			addChild(mQuotation);
		}

	}
}
