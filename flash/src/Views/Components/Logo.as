package Views.Components {
	import Lookups.BlueTrendyDynamicList;
	import Events.CustomSignals;
	import com.greensock.TweenMax;
	import Lookups.BlueTrendyStaticList;

	import flash.events.MouseEvent;
	/**
	 * @author LucasOK
	 */
	public class Logo extends logoMC {
		
		public function Logo() :void{
			//constructor
			ini();		
		}

		private function ini() : void {
			logoTextMC.visible = shadowMC.visible = false;	
			iconMC.mouseEnabled = logoTextMC.mouseEnabled = shadowMC.mouseEnabled = false;	
			
			alpha = .5;
			
			hitareaMC.id = BlueTrendyStaticList.MENU_HOME_BUTTON;
					
			fAddToStage();
		}

		private function fAddToStage() : void {
			
			fEnabled(true);
		}

		private function fEnabled(arg_b : Boolean) : void {
			if(arg_b){
				hitareaMC.addEventListener(MouseEvent.MOUSE_DOWN, fMouseDown);
				hitareaMC.addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				hitareaMC.addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			}else{
				hitareaMC.removeEventListener(MouseEvent.MOUSE_DOWN, fMouseDown);
				hitareaMC.removeEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
				hitareaMC.removeEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			
			}
		}

		private function fMouseOut(event :MouseEvent) : void {
			var t_nX :Number = BlueTrendyStaticList.APP_WIDTH - iconMC.width - BlueTrendyStaticList.APP_MARGIN;;
			logoTextMC.visible = shadowMC.visible = false;
			TweenMax.to(logoTextMC, .3, {autoAlpha:0});
			TweenMax.to(shadowMC, .3, {autoAlpha:0});
			TweenMax.to(this, .7,{alpha:.5, x: t_nX});
			buttonMode = false;		
//			this.x = 0;
		}

		private function fMouseOver(event :MouseEvent) : void {
			var t_nX :Number = BlueTrendyStaticList.APP_WIDTH - logoTextMC.x - logoTextMC.width - BlueTrendyStaticList.APP_MARGIN;
			logoTextMC.visible = shadowMC.visible = true;			
			TweenMax.to(this, .3,{alpha:1, x: t_nX});
			TweenMax.to(logoTextMC, .3, {autoAlpha:1});
			TweenMax.to(shadowMC, .3, {autoAlpha:1});
			buttonMode = true;		
//			this.x = -this.width;

		}

		private function fMouseDown(event :MouseEvent) : void {
			if(BlueTrendyDynamicList.CURRENT_PAGE == event.target.id) return;
//			BlueTrendyDynamicList.CURRENT_PAGE = event.target.id;
			CustomSignals.PAGE_CHANGE.dispatch(event.target.id);
		}
	}
}
