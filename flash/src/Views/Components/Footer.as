package Views.Components {
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyDynamicList;
	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Utils.ApplicationFonts;

	import Views.Base.CompositeViewBase;

	import com.greensock.TweenMax;

	import flash.display.MovieClip;
	import flash.display.StageDisplayState;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Footer extends CompositeViewBase {
		private var mContactBT 			: MovieClip;
		private var mClientsBT 			: MovieClip;
		private var mServicesBT 		: MovieClip;
		private var mFullScreenBT 		: MovieClip;
		private var mSocialNetwork 		: MovieClip;
		
		private var aButtons 			: Array;
		
		private var bFullScreen 		: Boolean;
		
		private var mFacebookBT 		: MovieClip;
		private var mTwitterBT 			: MovieClip;
		public function Footer(arg_model: Model, arg_controller: Controller):void {
			super(arg_model, arg_controller);
			iniListeners();
			ini();
		}

		private function iniListeners() : void {
			CustomSignals.STAGE_RESIZE.add(fPositions);
			CustomSignals.BUTTONS_ENABLED.add(fEnabled);
		}


		private function ini() : void {
			mClientsBT = fCreateButton(BlueTrendyStaticList.FOOTER_CLIENTS_BUTTON);
			mServicesBT = fCreateButton(BlueTrendyStaticList.FOOTER_SOLUTIONS_BUTTON);
			mContactBT = fCreateButton(BlueTrendyStaticList.FOOTER_CONTACT_BUTTON);
			
			aButtons = new Array(mClientsBT, mServicesBT, mContactBT);
			mFullScreenBT = fCreateFullScreenBT();
//			mFacebookBT = Buttons.fCreateIconButton(new facebookMC(), "facebook");
//			mTwitterBT = Buttons.fCreateIconButton(new twitterMc(), "facebook");
			mTwitterBT = new twitterMc();
			mFacebookBT = new facebookMC();
			
			mSocialNetwork = new MovieClip();
			
			bFullScreen = false;
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			for (var i : int = 0; i < aButtons.length; i++) {
				addChild(aButtons[i]);				
			}
			
			mFacebookBT.addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
			mFacebookBT.addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			
			mTwitterBT.addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
			mTwitterBT.addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
						
//			var mHitArea :MovieClip = new MovieClip();
//			mHitArea.graphics.beginFill(BlueTrendyStaticList.COLOR_WHITE);
//			mHitArea.graphics.drawRect(0, 0, BlueTrendyStaticList.APP_WIDTH, 60);
//			mHitArea.graphics.endFill();
//			
//			mHitArea.addEventListener(MouseEvent.MOUSE_OVER, fHitAreaOver);
//			mHitArea.addEventListener(MouseEvent.MOUSE_OVER, fHitAreaOut);
//			
//			mHitArea.y = -mHitArea.height;
			
//			addChild(mHitArea);
			mTwitterBT.alpha = .5;
			mFacebookBT.alpha = .5;

			mTwitterBT.x = mFacebookBT.width + BlueTrendyStaticList.FOOTER_MENU_SPACE_X;
			
			mSocialNetwork.addChild(mFacebookBT);
			mSocialNetwork.addChild(mTwitterBT);
			
			addChild(mSocialNetwork);
			addChild(mFullScreenBT);
			fPositions();
		}
		
		private function fEnabled(arg_b :Boolean) : void {
			if(arg_b){
				for (var i : int = 0; i < aButtons.length; i++) {
					aButtons[i].addEventListener(MouseEvent.CLICK, fMouseClick);
				}
				
			}else{
				for (var j : int = 0; j < aButtons.length; j++) {
					aButtons[j].removeEventListener(MouseEvent.CLICK, fMouseClick);
				}
			}
		}
		
		private function fPositions() : void {
			for (var i : int = 0; i < aButtons.length; i++) {
				if(i==0) aButtons[i].x = i; else aButtons[i].x = aButtons[i-1].x + aButtons[i-1].width + BlueTrendyStaticList.FOOTER_MENU_SPACE_X;
			}
			
			mFullScreenBT.x = BlueTrendyStaticList.APP_WIDTH - mFullScreenBT.width - (BlueTrendyStaticList.APP_MARGIN*2);
			mSocialNetwork.x = BlueTrendyStaticList.APP_WIDTH/2 - mSocialNetwork.width/2;
		}

//		private function fHitAreaOut(event : MouseEvent) : void {
//			TweenMax.to(this, .5, {alpha:.5});
//		}
//
//		private function fHitAreaOver(event : MouseEvent) : void {
//			TweenMax.to(this, .5, {alpha:1});
//		}

		private function fFullScreenBTClick(event : MouseEvent) : void {
			bFullScreen = !bFullScreen;
			trace("Lu: FullScreen " + bFullScreen);
			if(bFullScreen){
				BlueTrendyStaticList.APP_STAGE.displayState = StageDisplayState.FULL_SCREEN;
			}else{
				BlueTrendyStaticList.APP_STAGE.displayState = StageDisplayState.NORMAL;
			}
		}

		private function fFullScreenBTOut(event : MouseEvent) : void {
			TweenMax.to(event.target, .2, {autoAlpha:.5, tint: null});
			buttonMode = useHandCursor = false;
//			TweenMax.to((event.target as MovieClip).getChildAt(1), .2, {width:20, height:8});
		}

		private function fFullScreenBTOver(event : MouseEvent) : void {
			TweenMax.to(event.target, .2, {autoAlpha:1, tint: BlueTrendyStaticList.COLOR_GREY_DARK});
			buttonMode = useHandCursor = true;
//			TweenMax.to((event.target as MovieClip).getChildAt(1), .2, {width:28, height:16});
		}

		private function fCreateButton(arg_s :String) : MovieClip {
			var t_fFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, 11, 0x000000);
			var t_tName :TextField = ApplicationFonts.fCreateText(t_fFormat);
			var t_mButton :MovieClip = new MovieClip();
			
			t_tName.text = arg_s;
			t_tName.mouseEnabled = false;
			
			t_mButton.addEventListener(MouseEvent.MOUSE_OVER, fMouseOver);
			t_mButton.addEventListener(MouseEvent.MOUSE_OUT, fMouseOut);
			t_mButton.id = arg_s;
			
			t_mButton.alpha = .5;
			
			t_mButton.addChild(t_tName);
			
			return t_mButton;
		}
		
		private function fCreateFullScreenBT() : MovieClip {
			var t_mc:MovieClip = new MovieClip();
			var t_mBorder: MovieClip = new MovieClip();
			var t_mSquare: MovieClip = new MovieClip();
			
//			t_mBorder.graphics.beginFill(BlueTrendyStaticList.COLOR_GREY_DARK, 0);
			t_mBorder.graphics.lineStyle(2, BlueTrendyStaticList.COLOR_WHITE);
			t_mBorder.graphics.drawRect(2, 2, 22, 10);
			t_mBorder.graphics.endFill();
			
			t_mSquare.graphics.beginFill(BlueTrendyStaticList.COLOR_GREY_DARK);
			t_mSquare.graphics.drawRect(0, 0, 26, 14);
			t_mSquare.graphics.endFill();
			
//			t_mSquare.x = t_mBorder.width/2 - t_mSquare.width/2;
//			t_mSquare.y = t_mBorder.height/2 - t_mSquare.height/2;
			
			t_mBorder.mouseEnabled = false;
			t_mSquare.mouseEnabled = false;
			
			t_mc.alpha = .5;
			
			t_mc.addEventListener(MouseEvent.MOUSE_OVER, fFullScreenBTOver);
			t_mc.addEventListener(MouseEvent.MOUSE_OUT, fFullScreenBTOut);
			t_mc.addEventListener(MouseEvent.MOUSE_DOWN, fFullScreenBTClick);
			
			t_mc.addChild(t_mSquare);
			t_mc.addChild(t_mBorder);
			
			return t_mc;
		}

		private function fMouseClick(event :MouseEvent) : void {
			if(BlueTrendyDynamicList.CURRENT_PAGE == event.target.id) return;
//			BlueTrendyDynamicList.CURRENT_PAGE = event.target.id;
			CustomSignals.PAGE_CHANGE.dispatch(event.target.id);
		}

		private function fMouseOut(event : MouseEvent) : void {
			buttonMode = false;
			useHandCursor = false;
			event.target.alpha = .5;
		}

		private function fMouseOver(event : MouseEvent) : void {
			useHandCursor = true;
			buttonMode = true;
			event.target.alpha = 1;
		}
	}
}
