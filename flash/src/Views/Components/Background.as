package Views.Components {
	import Controller.Controller;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Views.Base.CompositeViewBase;

	import com.greensock.TweenMax;

	import flash.display.Loader;
	import flash.events.Event;
	import flash.events.IOErrorEvent;
	import flash.events.ProgressEvent;
	import flash.net.URLRequest;
	/**
	 * @author user
	 */
	public class Background extends CompositeViewBase {
		private var lLoader : Loader;
		
		public function Background(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			ini();
		}

		private function ini() : void {
			lLoader = new Loader();
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			var t_sRequest :String = oModel.background;
			lLoader.load(new URLRequest(t_sRequest));
			lLoader.contentLoaderInfo.addEventListener(Event.COMPLETE, fComplete);
			lLoader.contentLoaderInfo.removeEventListener(ProgressEvent.PROGRESS, fLoadProgress);
			lLoader.contentLoaderInfo.addEventListener(IOErrorEvent.IO_ERROR, fLoadError);
		}
		
		private function fLoadProgress(event :ProgressEvent) : void {
			var percent :int = event.bytesLoaded / event.bytesTotal * 100;
//			trace("progress" + percent + "%");
		}

		private function fComplete(event : Event) : void {
			this.x = BlueTrendyStaticList.APP_WIDTH/2 - lLoader.width/2;
			TweenMax.from(lLoader, .3, {alpha:0});
			oController.fStartLoad(true);
			addChild(lLoader);
		}

		private function fLoadError(event :IOErrorEvent) : void {
			trace(event.text);
		}
	}
}
