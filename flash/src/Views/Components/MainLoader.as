package Views.Components {
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;
	import Models.VO.ImageVO;
	import Models.VO.WindowVO;

	import Views.Base.CompositeViewBase;

	import br.com.stimuli.loading.BulkLoader;
	import br.com.stimuli.loading.BulkProgressEvent;

	import com.greensock.TweenMax;
	import com.greensock.easing.Linear;
	import com.greensock.plugins.TransformAroundPointPlugin;
	import com.greensock.plugins.TweenPlugin;

	import flash.display.MovieClip;
	/**
	 * @author LucasOK
	 */
	
	public class MainLoader extends CompositeViewBase {
		TweenPlugin.activate([TransformAroundPointPlugin]);
		
		private var mLoader 				: loaderMC;
		private var mMask 					: MovieClip;
		private var mMainLoader 			: miniLoaderMC;
		
		private var lLoader : BulkLoader;
		private var mLine : MovieClip;
		
		public function MainLoader(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			//constructor 
			ini();
		}

		private function ini() : void {
			mLoader = new loaderMC();
			mMainLoader = new miniLoaderMC();
			mMask = new MovieClip();
			mLine = new MovieClip();
			
			lLoader = new BulkLoader();
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			mLoader.width = BlueTrendyStaticList.APP_WIDTH;
			
			mMask.graphics.beginFill(0x000000);
			mMask.graphics.drawRect(0, 0, mLoader.width, mLoader.height);
			mMask.graphics.endFill();
			
			mLine.graphics.beginFill(BlueTrendyStaticList.COLOR_GREY_MEDIUM);
			mLine.graphics.drawRect(0, 0, mLoader.width, mLoader.height);
			mLine.graphics.endFill();
			
			mMainLoader.alpha = 0;
			mMask.width = 0;
			
			mLoader.y = BlueTrendyStaticList.APP_HEIGHT/2 - mLoader.height/2;
			mMask.y = BlueTrendyStaticList.APP_HEIGHT/2 - mMask.height/2;
			mLine.y = BlueTrendyStaticList.APP_HEIGHT/2 - mMask.height/2;
			
			mMainLoader.x = BlueTrendyStaticList.APP_WIDTH/2;
			mMainLoader.y = mLoader.y - mMainLoader.height/2 - BlueTrendyStaticList.APP_MARGIN;
			
			mLoader.mask = mMask;
			
			TweenMax.from(mLine, 1, {width:0, ease:Linear.easeNone});
			TweenMax.to(mMainLoader, .5, {alpha:1, delay:1.2});
			
			addChild(mLine);
			addChild(mMask);
			addChild(mLoader);
			addChild(mMainLoader);

			fStartLoad();
		}

		private function fStartLoad() : void {
//			 lLoader.logLevel = BulkLoader.LOG_INFO;
			for (var i : int = 0; i < oModel.portfolio.length; i++) {
				var t_aPortfolio :Array = oModel.portfolio[i];
				for (var j : int = 0; j < t_aPortfolio.length; j++) {
					var t_oWindowVO :WindowVO = t_aPortfolio[j];
					var t_oImageVO :ImageVO = t_oWindowVO.images[0] as ImageVO;
					var	t_sThumbSrc :String = t_oImageVO.thumb; 
					
					lLoader.add(t_sThumbSrc);
				}
			}
			for (var u : int = 0; u < oModel.clientsVO.logos.length; u++) {
				var t_sClients :String = oModel.clientsVO.logos[u];
				lLoader.add(t_sClients);
			}
			
			lLoader.addEventListener(BulkLoader.COMPLETE, fOnAllItemsLoaded);
            lLoader.addEventListener(BulkLoader.PROGRESS, fOnAllItemsProgress);

			lLoader.start();
		}

		private function fOnAllItemsProgress(event :BulkProgressEvent) : void {
			mMask.width = mLoader.width*event.weightPercent;
			trace("loadingStatus " + event.weightPercent*100, mLoader.width*(event.weightPercent));
		}

		private function fOnAllItemsLoaded(event :BulkProgressEvent) : void {
			removeChild(mLine);
			TweenMax.to(mMainLoader, .5, {autoAlpha:0});
			TweenMax.to(mLoader, .5, {colorTransform: {tint:BlueTrendyStaticList.COLOR_GREY_MEDIUM, tintAmount:1}});
			TweenMax.to(mMask, 1, {x:BlueTrendyStaticList.APP_WIDTH ,ease:Linear.easeNone, delay:1, onComplete:fStartWebsite});
		}

		private function fStartWebsite() : void {
			removeChild(mMask);
			removeChild(mLoader);
			CustomSignals.WEBSITE_LOADED.dispatch();
		}
	}
}
