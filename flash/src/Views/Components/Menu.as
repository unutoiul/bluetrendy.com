package Views.Components {
	import Controller.Controller;

	import Events.CustomSignals;

	import Lookups.BlueTrendyDynamicList;
	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Utils.Buttons;

	import Views.Base.CompositeViewBase;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	/**
	 * @author LucasOK
	 */
	public class Menu extends CompositeViewBase {
		private var mFlashButton 		: MovieClip;
		private var mHtmlButton 	: MovieClip;
		private var aButtons 			: Array;
		private var mMobileButton 		: MovieClip;
		
		public function Menu(arg_model: Model, arg_controller: Controller):void {
			super(arg_model, arg_controller);
			iniListeners();
			ini();
		}

		private function iniListeners() : void {
			CustomSignals.BUTTONS_ENABLED.add(fEnabled);
		}

		private function ini() : void {
			mFlashButton = Buttons.fCreateMenuButton(BlueTrendyStaticList.MENU_FLASH_BUTTON, "Websites/Games");
			mHtmlButton = Buttons.fCreateMenuButton(BlueTrendyStaticList.MENU_HTML_BUTTON, "Websites/Apps");
			mMobileButton = Buttons.fCreateMenuButton(BlueTrendyStaticList.MENU_MOBILE_BUTTON, "iPhone/Android/iPad");
			
			aButtons = new Array(mHtmlButton, mFlashButton);
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			for (var i : int = 0; i < aButtons.length; i++) {
				if(i==0) aButtons[i].x = i; else aButtons[i].x = aButtons[i-1].x + aButtons[i-1].width + BlueTrendyStaticList.MENU_SPACE_X;

				addChild(aButtons[i]);
			}
		}
		
		private function fEnabled(arg_b :Boolean):void{
			if(arg_b){
				for (var i : int = 0; i < aButtons.length; i++) {
					aButtons[i].addEventListener(MouseEvent.MOUSE_DOWN, fButtonClicked);
				}				
			}else{
				for (var j : int = 0; j < aButtons.length; j++) {
					aButtons[j].removeEventListener(MouseEvent.MOUSE_DOWN, fButtonClicked);
				}
			}
		}

		private function fButtonClicked(event : MouseEvent) : void {
			if(BlueTrendyDynamicList.CURRENT_PAGE == event.target.parent.id) return;
//			BlueTrendyDynamicList.CURRENT_PAGE = event.target.parent.id;
			CustomSignals.PAGE_CHANGE.dispatch(event.target.parent.id);
		}
	}
}
