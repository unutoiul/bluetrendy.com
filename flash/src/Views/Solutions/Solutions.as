package Views.Solutions {
	import Controller.Controller;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;
	import Models.VO.SectionVO;

	import Utils.ApplicationFonts;
	import Utils.Buttons;

	import Views.Base.CompositeViewBase;

	import com.greensock.TweenMax;

	import flash.display.MovieClip;
	import flash.events.MouseEvent;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Solutions extends CompositeViewBase {
		private var mSecoundLine 		: MovieClip;
		private var mSubTitle 			: MovieClip;
		private var mFirstLine 			: MovieClip;
		private var mDummyLine 			: MovieClip;
		
		private var fTitle 				: TextFormat;
		private var fSubTitle 			: TextFormat;
		private var fBody 				: TextFormat;
		
		private var tTitle 				: TextField;
		private var tSubTitle 			: TextField;
		
		private var aSections 			: Array;
		private var aSectionsMask 		: Array;
		private var aSectionsName 		: Array;
		
		private var nCurrentSection 	: Number;
		private var nSectionNameHeight 	: Number;

		public function Solutions(arg_model: Model, arg_controller: Controller):void {
			super(arg_model , arg_controller);
			ini();
		}

		private function ini() : void {
			fTitle = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_TITLE_FONT_SIZE, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			fSubTitle = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_SUBTITLE_FONT_SIZE, BlueTrendyStaticList.COLOR_YELLOW, "right");
			fBody = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaM, BlueTrendyStaticList.PAGE_BODY_FONT_SIZE, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			
			tTitle = ApplicationFonts.fCreateText(fTitle);
			tSubTitle = ApplicationFonts.fCreateText(fSubTitle);
			
			mFirstLine = fCreateLine(1);
			mSecoundLine = fCreateLine(5);
			mDummyLine = fCreateLine(1);
			mSubTitle = fCreateSlogan();
			
			nCurrentSection = 0;
			
			fAddToStage();
		}

		private function fAddToStage() : void {
			tTitle.text = oModel.solutionsVO.title;
			tSubTitle.text = oModel.solutionsVO.subtitle;
			mFirstLine.y = tTitle.height;
			mSecoundLine.y = 250;
			
			tTitle.x = BlueTrendyStaticList.PAGE_COLUMN_WIDTH;
			tTitle.selectable = tSubTitle.selectable = true;

			tSubTitle.multiline = true;
			tSubTitle.wordWrap = true;
			tSubTitle.width = 320;

			mSubTitle.x = BlueTrendyStaticList.PAGE_LINE_WIDTH - tSubTitle.width;
			mSubTitle.y =  mSecoundLine.y/2 - tSubTitle.height/2 + mFirstLine.y/2;
			
			//Dummy line for Position
			mDummyLine.y = 400;
			mDummyLine.visible = false;
			addChild(mDummyLine);
			
			addChild(mFirstLine);
			addChild(mSecoundLine);
			addChild(tTitle);
			addChild(mSubTitle);

			fCreateSections();
//			trace("tTitle.y " + tTitle.y , mFirstLine.x, mFirstLine.y);
		}

		private function fCreateSections() : void {
			var t_aSectionsVO :Array = oModel.solutionsVO.section as Array;
			
			aSections = new Array();
			aSectionsName = new Array();
			aSectionsMask = new Array();
			
			for (var i : int = 0; i < t_aSectionsVO.length; i++) {
				var t_mMask :MovieClip = new MovieClip();
				var t_tSection :TextField = ApplicationFonts.fCreateText(fBody);
				var t_tSectionName :MovieClip = Buttons.fCreateArrowButton((t_aSectionsVO[i] as SectionVO).name, true);
				
				t_tSection.condenseWhite = true;
				t_tSection.multiline = true;
				t_tSection.wordWrap = true;
				
				t_tSection.width = BlueTrendyStaticList.PAGE_LINE_WIDTH;
				
				t_tSection.htmlText = (t_aSectionsVO[i] as SectionVO).body;
				t_tSection.selectable = true;
				
				t_mMask.graphics.beginFill(BlueTrendyStaticList.COLOR_BLUE_LIGHT);
				t_mMask.graphics.drawRect(0, 0, t_tSection.width, t_tSection.height);
				t_mMask.graphics.endFill();
				
				t_tSectionName.x = t_tSection.x = BlueTrendyStaticList.PAGE_COLUMN_WIDTH;
				
				t_tSectionName.id = i;
				t_tSection.mask = t_mMask;
				nSectionNameHeight = t_tSectionName.height;
				
				aSections.push(t_tSection);
				aSectionsName.push(t_tSectionName);
				aSectionsMask.push(t_mMask);

				addChild(aSectionsMask[i]);
				addChild(aSectionsName[i]);
				addChild(aSections[i]);
			}
			fSectionEnabled(true);
			fOpenSection(nCurrentSection);
		}

		private function fSectionEnabled(arg_b : Boolean) : void {
			if(arg_b){
				for (var i : int = 0; i < aSectionsName.length; i++) {
					(aSectionsName[i] as MovieClip).addEventListener(MouseEvent.MOUSE_DOWN, fSelectSection);
				}
			}else{
				for (var j : int = 0; j < aSectionsName.length; j++) {
					(aSectionsName[j] as MovieClip).removeEventListener(MouseEvent.MOUSE_DOWN, fSelectSection);
				}
			}
		}

		private function fSelectSection(event :MouseEvent) : void {
			trace("fSelectSection " + event.target.parent.id);
			nCurrentSection = event.target.parent.id;
			fOpenSection(nCurrentSection);
		}

		private function fOpenSection(arg_n : Number) : void {
			for (var i : int = 0; i < aSections.length; i++) {
				if(i == nCurrentSection){
					TweenMax.to(aSections[i], 0, {autoAlpha:1});
					TweenMax.to(aSectionsMask[i], .5, {height:aSections[i].height, onUpdate:fPosSections});
					aSectionsName[i].mouseEnabled = aSectionsName[i].mouseChildren = false;
				}else{
					TweenMax.to(aSections[i], .2, {autoAlpha:0});
					TweenMax.to(aSectionsMask[i], .5, {height:0});
					aSectionsName[i].mouseEnabled = aSectionsName[i].mouseChildren = true;
				}
			}
		}

		private function fPosSections() : void {
			for (var i : int = 0; i < aSections.length; i++) {
				if(i==0) aSectionsName[i].y = tTitle.height; else aSectionsName[i].y = aSectionsName[i-1].y + aSectionsMask[i-1].height + BlueTrendyStaticList.SOLUTIONS_MARGIN;
				if(i==0) aSections[i].y = aSectionsName[i].y + nSectionNameHeight; else aSections[i].y = aSectionsMask[i-1].y + aSectionsMask[i-1].height + BlueTrendyStaticList.SOLUTIONS_MARGIN;

				aSectionsMask[i].x = aSections[i].x;
				aSectionsMask[i].y = aSections[i].y;
			}
		}

		private function fCreateSlogan() : MovieClip {
			var t_mSubTitle :MovieClip = new MovieClip();
			t_mSubTitle.addChild(tSubTitle);
			TweenMax.to(t_mSubTitle, 0., {dropShadowFilter:{color:BlueTrendyStaticList.COLOR_BLACK_LIGHT, alpha:1, blurX:2, blurY:2, strength:1, distance:3}});
			return t_mSubTitle;
		}
		
		private function fCreateLine(arg_h:Number) : MovieClip {
			var t_sLine :MovieClip = new MovieClip();
			t_sLine.graphics.beginFill(BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			t_sLine.graphics.drawRect(0, 0, BlueTrendyStaticList.PAGE_LINE_WIDTH, arg_h);
			t_sLine.graphics.endFill();
			return t_sLine;
		}
	}
}
