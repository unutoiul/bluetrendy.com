package Events {
	import org.osflash.signals.Signal;

	import flash.display.MovieClip;
	/**
	 * @author LucasOK
	 */
	public class CustomSignals{
		public static const STAGE_RESIZE						:Signal	= new Signal();

		public static const LOAD_XML_SUCCESS					:Signal	= new Signal(Boolean);
		public static const BUTTONS_ENABLED						:Signal	= new Signal(Boolean);
		
		public static const SHOW_MAIN_LOADER					:Signal	= new Signal(Boolean);
//		public static const UPDATE_MAIN_LOADER					:Signal	= new Signal(Number);
		public static const WEBSITE_LOADED						:Signal	= new Signal();
		
		public static const PAGE_CHANGE							:Signal	= new Signal(String);
		
		public static const PORTFOLIO_GRID_FORMAT				:Signal	= new Signal();
		public static const PORTFOLIO_THUMB_LOADED				:Signal	= new Signal();
		public static const PORTFOLIO_THUMB_CLICK				:Signal	= new Signal(MovieClip);
		public static const PORTFOLIO_ICON_CLICK				:Signal	= new Signal(int);

		public static const PORTFOLIO_IMAGE_LOADER				:Signal	= new Signal(Boolean);
		public static const PORTFOLIO_IMAGE_LOADED				:Signal	= new Signal(Boolean);
		
		public static const FORM_SEND_SUCCESS					:Signal	= new Signal(Boolean);
		
//		PORTFOLIO_IMAGE_LOADED.
	}
}
