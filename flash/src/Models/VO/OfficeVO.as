package Models.VO {
	/**
	 * @author LucasOK
	 */
	public class OfficeVO {
		private var sLocation 			: String;
		private var sAddress 			: String;
		private var sPerson 			: String;
		private var sEmail				: String;
		private var sTelephone 			: String;
		
		public function OfficeVO(arg_loc :String, arg_address :String, arg_person :String, arg_email :String, arg_telephone :String) :void{
			sLocation = arg_loc;
			sAddress = arg_address;
			sPerson = arg_person;
			sEmail = arg_email;
			sTelephone = arg_telephone;
		}

		public function get location() : String {
			return sLocation;
		}

		public function set location(location : String) : void {
			sLocation = location;
		}

		public function get address() : String {
			return sAddress;
		}

		public function set address(address : String) : void {
			sAddress = address;
		}

		public function get person() : String {
			return sPerson;
		}

		public function set person(person : String) : void {
			sPerson = person;
		}

		public function get email() : String {
			return sEmail;
		}

		public function set email(email : String) : void {
			sEmail = email;
		}

		public function get telephone() : String {
			return sTelephone;
		}

		public function set telephone(telephone : String) : void {
			sTelephone = telephone;
		}
	}
}
