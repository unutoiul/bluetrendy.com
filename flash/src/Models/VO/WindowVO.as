package Models.VO {
	/**
	 * @author LucasOK
	 */
	public class WindowVO {
		private var nId						: String;
		private var sName 					: String;
		private var sShortDesc 				: String;
		private var sLink 					: String;
		private var aImages 				: Array;
		private var sDesc 					: String;
		private var sClient 				: String;
		private var sProductionTime 		: String;
		private var sTechnologies 			: String;
		
		public function WindowVO(arg_id:String, arg_name:String, arg_shortdesc:String, arg_link:String, arg_desc:String, arg_client:String, arg_productiontime:String, arg_technologies:String, arg_images:Array) :void{
			nId = arg_id;
			sName = arg_name;
			sShortDesc = arg_shortdesc;
			sLink = arg_link;
			sDesc = arg_desc;
			sClient = arg_client;
			sProductionTime = arg_productiontime;
			sTechnologies = arg_technologies;
			aImages = arg_images;
		}

		public function get id() : String {
			return nId;
		}

		public function get name() : String {
			return sName;
		}

		public function get images() : Array {
			return aImages;
		}

		public function get shortDesc() : String {
			return sShortDesc;
		}

		public function get link() : String {
			return sLink;
		}

		public function get desc() : String {
			return sDesc;
		}

		public function get client() : String {
			return sClient;
		}

		public function get productionTime() : String {
			return sProductionTime;
		}

		public function get technologies() : String {
			return sTechnologies;
		}
	}
}
