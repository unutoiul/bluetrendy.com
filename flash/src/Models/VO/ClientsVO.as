package Models.VO {
	/**
	 * @author LucasOK
	 */
	public class ClientsVO {
		private var sTitle 				: String;
		private var sSubtitle 			: String;
		private var sBody 				: String;
		private var aLogos 				: Array;
		
		public function ClientsVO(arg_title :String, arg_subtitle :String, arg_body :String, arg_logos :Array) :void{
			sTitle = arg_title;
			sSubtitle = arg_subtitle;
			sBody = arg_body;
			aLogos = arg_logos;
		}

		public function get title() : String {
			return sTitle;
		}

		public function set title(title : String) : void {
			sTitle = title;
		}

		public function get subtitle() : String {
			return sSubtitle;
		}

		public function set subtitle(subtitle : String) : void {
			sSubtitle = subtitle;
		}


		public function get body() : String {
			return sBody;
		}

		public function set body(body : String) : void {
			sBody = body;
		}
		
		public function get logos() : Array {
			return aLogos;
		}

		public function set logos(logos : Array) : void {
			aLogos = logos;
		}

	}
}
