package Models.VO {
	/**
	 * @author LucasOK
	 */
	public class SolutionsVO {
		private var sTitle 					: String;
		private var sSubtitle 				: String;
		private var aSection 				: Array;
		
		public function SolutionsVO(arg_title :String, arg_subtitle :String, arg_section :Array) :void{
			sTitle = arg_title;
			sSubtitle = arg_subtitle;
			aSection = arg_section;
		}

		public function get title() : String {
			return sTitle;
		}

		public function set title(title : String) : void {
			sTitle = title;
		}

		public function get subtitle() : String {
			return sSubtitle;
		}

		public function set subtitle(subtitle : String) : void {
			sSubtitle = subtitle;
		}

		public function get section() : Array {
			return aSection;
		}

		public function set section(section : Array) : void {
			aSection = section;
		}
	}
}
