package Models.VO {
	/**
	 * @author LucasOK
	 */
	public class ImageVO {
		private var sSrc 		: String;
		private var sThumb 		: String;
		private var sIcon 		: String;
		
		public function ImageVO(arg_src:String, arg_thumb:String, arg_icon:String) :void{
			sSrc = arg_src;
			sThumb = arg_thumb;
			sIcon = arg_icon;
		}

		public function get src() : String {
			return sSrc;
		}

		public function set src(arg_src : String) : void {
			sSrc = arg_src;
		}

		public function get thumb() : String {
			return sThumb;
		}

		public function set thumb(arg_thumb : String) : void {
			sThumb = arg_thumb;
		}

		public function get icon() : String {
			return sIcon;
		}

		public function set icon(arg_icon : String) : void {
			sIcon = arg_icon;
		}
	}
}
