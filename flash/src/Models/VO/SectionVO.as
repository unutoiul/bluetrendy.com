package Models.VO {
	/**
	 * @author LucasOK
	 */
	public class SectionVO {
		private var sBody 				: String;
		private var sName 				: String;
		
		public function SectionVO(arg_name :String, arg_body :String) :void{
			sName = arg_name;
			sBody = arg_body;
		}


		public function get body() : String {
			return sBody;
		}

		public function set body(body : String) : void {
			sBody = body;
		}

		public function get name() : String {
			return sName;
		}

		public function set name(name : String) : void {
			sName = name;
		}
	}
}

