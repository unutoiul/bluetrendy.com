package Models {
	import Models.VO.SectionVO;
	import Models.VO.ClientsVO;
	import Models.VO.SolutionsVO;
	import Models.VO.ImageVO;
	import Models.VO.OfficeVO;
	import Models.VO.WindowVO;

	import flash.events.EventDispatcher;
	/**
	 * @author LucasOK
	 */
	public class Model extends EventDispatcher {
		private var oData 						: XML;
		private var aMobileVO 					: Array;
		private var aOfficesVO 					: Array;
		private var aImagesVO	 				: Array;
		private var aSectionVO	 				: Array;
		private var aLogos		 				: Array;
		private var aPortfolio 					: Array;
		
		private var oSolutionsVO 				: SolutionsVO;
		private var oClientsVO 					: ClientsVO;
		
		private var sBackground 				: String;
		private var sHomeTitle 					: String;
		private var aHtmlVO 					: Array;
		private var aFlashVO 					: Array;
		
		public function Model(arg_data: XML = null){
			//constructor
			if( arg_data != null ) oData = arg_data;
			
			ini();
		}
		
		public function ini():void {
			var childNodes:XMLList = oData.children();

			aHtmlVO = new Array(); 
			aFlashVO = new Array(); 
			aMobileVO = new Array(); 
			aOfficesVO = new Array(); 
			aImagesVO = new Array(); 
			aSectionVO = new Array(); 
			aLogos = new Array(); 
			aPortfolio = new Array(); 
			
			sBackground = oData.@bg;
			
//			trace("oData.@config " + oData.@bg);
			
			for each(var childXML:XML in childNodes){
				switch(childXML.name().localName){
					case "home":
					collectXMLData(childXML);
					break;
					
					case "portfolio":
					collectXMLData(childXML);
					aPortfolio.push(aHtmlVO, aFlashVO);
					break;
					
					case "solutions":
					collectXMLData(childXML);
					var t_vSolutions:SolutionsVO = new SolutionsVO(childXML.title, childXML.subtitle, aSectionVO);
					oSolutionsVO = t_vSolutions;
					break;
					
					case "clients":
					collectXMLData(childXML);
					var t_vClients:ClientsVO = new ClientsVO(childXML.title, childXML.subtitle, childXML.body, aLogos);
					oClientsVO = t_vClients;
					break;
					
					case "contact":
					collectXMLData(childXML);
					break;
					
					default:
					break;
				}
			}
		}
		
		private function collectXMLData(arg_data:XML):void {
			var childNodes:XMLList = arg_data.children();
			
			for each(var childXML:XML in childNodes){
				switch(childXML.name().localName){
					case "website":
					aImagesVO = [];
					collectXMLDataChildren(childXML);
					var t_vHtml:WindowVO = new WindowVO(childXML.@id, childXML.@name, childXML.@desc, childXML.@link, childXML.desc, childXML.client, childXML.productiontime, childXML.technologies, aImagesVO);
					aHtmlVO.push(t_vHtml);
					break;
					
					case "game":
					aImagesVO = [];
					collectXMLDataChildren(childXML);
					var t_vFlash:WindowVO = new WindowVO(childXML.@id, childXML.@name, childXML.@desc, childXML.@link, childXML.desc, childXML.client, childXML.productiontime, childXML.technologies, aImagesVO);
					aFlashVO.push(t_vFlash);
					break;
					
					case "mobile":
					aImagesVO = [];
					collectXMLDataChildren(childXML);
					var t_vMobile:WindowVO = new WindowVO(childXML.@id, childXML.@name, childXML.@desc, childXML.@link, childXML.desc, childXML.client, childXML.productiontime, childXML.technologies, aImagesVO);
					aMobileVO.push(t_vMobile);
					break;
					
					case "office":
					var t_vOffice:OfficeVO = new OfficeVO(childXML.location, childXML.address, childXML.person, childXML.email, childXML.telephone);
					aOfficesVO.push(t_vOffice);
					break;
					
					case "logo":
					aLogos.push(childXML.@src);
					break;
					
					case "title":
					sHomeTitle = childXML;
					break;
					
					case "section":
					var t_vSection:SectionVO = new SectionVO(childXML.@name, childXML);
					aSectionVO.push(t_vSection);
					break;
					
					default:
				}
			}
		}
		
		private function collectXMLDataChildren(arg_data:XML):void {
			var childNodes:XMLList = arg_data.children();
			
			for each(var childXML:XML in childNodes){
				switch(childXML.name().localName){
					case "img":
					var t_vImage:ImageVO = new ImageVO(childXML.@src, childXML.@thumb, childXML.@icon);
					aImagesVO.push(t_vImage);
					break;
//						
					default:
				}
			}
		}

		public function get background() : String {
			return sBackground;
		}
		
		public function get homeTitle() : String {
			return sHomeTitle;
		}

		public function get officesVO() : Array {
			return aOfficesVO;
		}

		public function get solutionsVO() : SolutionsVO {
			return oSolutionsVO;
		}

		public function get clientsVO() : ClientsVO {
			return oClientsVO;
		}

		public function get mobileVO() : Array {
			return aMobileVO;
		}

		public function get portfolio() : Array {
			return aPortfolio;
		}

		public function get htmlVO() : Array {
			return aHtmlVO;
		}

		public function get flashVO() : Array {
			return aFlashVO;
		}
	}
}
