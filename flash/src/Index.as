package {
	import Events.CustomSignals;

	import Lookups.BlueTrendyStaticList;

	import Models.Model;

	import Utils.XmlFileLoader;

	import Views.View;

	import flash.display.MovieClip;
	import flash.display.StageAlign;
	import flash.display.StageScaleMode;
	import flash.events.Event;
	/**
	 * @author Lucas Franco
	 */
	[SWF(backgroundColor="#FFFFFF", width="1024", height="600", frameRate="30")]
	public class Index extends MovieClip{
		private var xmlFileLoader 			: XmlFileLoader;
		private var configXML 				: XML;
		private var mView 					: View;
		
		public function Index() :void{
			addEventListener(Event.ADDED_TO_STAGE, ini);
//			SWFProfiler.init(stage, this);
		}

		private function fResizeListener(event : Event) : void {
			BlueTrendyStaticList.APP_WIDTH = stage.stageWidth;
			BlueTrendyStaticList.APP_HEIGHT = stage.stageHeight;
			
			CustomSignals.STAGE_RESIZE.dispatch();
		}

		private function ini(event: Event) : void {
			BlueTrendyStaticList.APP_STAGE = stage;
			
			BlueTrendyStaticList.APP_STAGE.showDefaultContextMenu = false;
			BlueTrendyStaticList.APP_STAGE.scaleMode = StageScaleMode.NO_SCALE;
			BlueTrendyStaticList.APP_STAGE.align = StageAlign.TOP_LEFT;
			BlueTrendyStaticList.APP_STAGE.addEventListener(Event.RESIZE, fResizeListener);
			
			fResizeListener(null);
			fAddToStage();
		}
		
		private function fAddToStage() : void {
			loadConfigXML();
		}
		
		public function loadConfigXML() : void {
			trace("LOADING: loading config XML "  + BlueTrendyStaticList.CONFIG_FILENAME);
			
			xmlFileLoader = new XmlFileLoader();
			xmlFileLoader.fLoad(BlueTrendyStaticList.CONFIG_FILENAME);
			
			CustomSignals.LOAD_XML_SUCCESS.add(xmlLoadHandler);
		}

		private function xmlLoadHandler(arg_b :Boolean) : void {
			if(arg_b){
				configXML = xmlFileLoader.getLoadedXML();
//				trace("Document Class:XML Loaded Successfully" + configXML);
				fIniModelXML();
			
			}else{
				trace("Document Class:Error Loading XML");
			}
		}

		private function fIniModelXML() : void {
			fIniMVC();
		}

		private function fIniMVC() : void {
			var t_oModel :Model = new Model(configXML);
			mView = new View(t_oModel);
			
			
			CustomSignals.STAGE_RESIZE.dispatch();
			addChild(mView);
		}
	}
}
