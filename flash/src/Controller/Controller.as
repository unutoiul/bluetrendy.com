package Controller {
	import Events.CustomSignals;
	import Models.Model;

	import Views.View;
	/**
	 * @author LucasOK
	 */
	public class Controller {
		private var oModel : Model;
		private var oView : View;
		
//		 * class constructor
//		 * the CONTROLLER holds a reference of the MODEL
		public function Controller(arg_model:Model, arg_view :View)	{
			oModel = arg_model;
			oView = arg_view;
		}
		
		public function ini():void{
		
		}
		
		public function fStartLoad(arg_b: Boolean):void{
			oView.fShowLoader(arg_b);
		}
	}
}
