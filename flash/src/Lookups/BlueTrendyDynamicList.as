package Lookups {
	/**
	 * @author LucasOK
	 */
	public class BlueTrendyDynamicList {
		private static var _CURRENT_PAGE : String;

		public static function get CURRENT_PAGE() : String {
			return _CURRENT_PAGE;
		}

		public static function set CURRENT_PAGE(arg_s : String) : void {
			_CURRENT_PAGE = arg_s;
		}
	}
}
