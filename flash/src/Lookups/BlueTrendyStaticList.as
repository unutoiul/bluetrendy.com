package Lookups {
	import flash.display.Stage;
	/**
	 * @author LucasOK
	 */
	public class BlueTrendyStaticList {
		public static var CONFIG_FILENAME 									:String =  "xml/config.xml";
		
//		APP SIZE & STAGE
		public static var APP_STAGE 										:Stage;
		public static var APP_WIDTH 										:Number =  1024;
		public static var APP_HEIGHT 										:Number =  600;
		
		public static var APP_MARGIN 										:Number =  15;
		
		public static var MAIL_GATEWAY										:String = "http://www.bluetrendy.com/assets/mail.php";
		
		public static var SECRET_KEY										:String = "X9gV1d8zWsMM";	
		
		public static var COLOR_BLUE 										:uint =  0x4180AE;
		public static var COLOR_BLUE_LIGHT									:uint =  0x3399FF;
		public static var COLOR_BLUE_DARK									:uint =  0x21445B;
		public static var COLOR_YELLOW										:uint =  0xFAD610;
		public static var COLOR_RED											:uint =  0x990000;
		public static var COLOR_BLACK_LIGHT 								:uint =  0x111111;
		public static var COLOR_GREY_DARK 									:uint =  0x1A1A1A;
		public static var COLOR_GREY_MEDIUM 								:uint =  0x444444;
		public static var COLOR_GREY_LIGHT 									:uint =  0x9A9A9A;
		public static var COLOR_GREY_VERY_LIGHT 							:uint =  0xEEEEEE;
		public static var COLOR_WHITE	 									:uint =  0xFFFFFF;

		public static var MENU_SPACE_X			 							:Number =  20;
		public static var FOOTER_MENU_SPACE_X	 							:Number =  10;
		
		public static var PAGE_TITLE_FONT_SIZE								:Number =  36;
		public static var PAGE_SUBTITLE_FONT_SIZE							:Number =  42;
		public static var PAGE_BUTTON_FONT_SIZE								:Number =  16;
		public static var PAGE_BODY_FONT_SIZE								:Number =  12;
		public static var PAGE_LINE_WIDTH									:Number =  425;
		public static var PAGE_COLUMN_WIDTH									:Number =  500;
		
		public static var PORTFOLIO_ROTATE_TIME 							:Number =  .7;
		public static var PORTFOLIO_ALPHA_BLUR_TIME 						:Number =  1;
		
		public static var PORTFOLIO_THUMB_WIDTH 							:Number =  170;
		public static var PORTFOLIO_THUMB_HEIGHT 							:Number =  100;
		public static var PORTFOLIO_THUMB_INFO_H 							:Number =  56;
		public static var PORTFOLIO_THUMB_DIST 								:Number =  9;
		public static var PORTFOLIO_THUMB_ANGLE_START 						:Number =  110;
		public static var PORTFOLIO_THUMB_ANGLE_VIEW 						:Number =  70;
		public static var PORTFOLIO_THUMB_BLUR 								:Number =  2;
		public static var PORTFOLIO_THUMB_ROTATE 							:Number =  6;
		public static var PORTFOLIO_SWAP_TIME 								:Number =  10;
//		
		public static var PORTFOLIO_CIRCLE_RADIUS 							:Number =  300;
		public static var PORTFOLIO_CIRCLE_X 								:Number =  300;
		public static var PORTFOLIO_CIRCLE_Y 								:Number =  70;
		public static var PORTFOLIO_TEXT_X 									:Number =  450;
		public static var PORTFOLIO_DESC_Y									:Number =  18;
		public static var PORTFOLIO_MARGIN									:Number =  5;
		public static var PORTFOLIO_TEXT_MARGIN								:Number =  3;
		public static var PORTFOLIO_GRID_DISTANCE							:Number =  15;
		
		public static var PORTFOLIO_IMAGE_WIDTH 							:Number =  680;
		public static var PORTFOLIO_IMAGE_HEIGHT 							:Number =  400;
		
		public static var PORTFOLIO_NAV_MARGIN 								:Number =  100;
		
		public static var PORTFOLIO_ICON_X	 								:Number =  150;
		public static var PORTFOLIO_ICON_WIDTH 								:Number =  40;
		public static var PORTFOLIO_ICON_HEIGHT 							:Number =  40;
		
		public static var PORTFOLIO_INFOTEXT_MARGIN							:Number =  15;
		public static var PORTFOLIO_INFOTEXT_DESC_Y							:Number =  100;
		
		public static var SOLUTIONS_MARGIN									:Number =  30;
		
		
		
		public static var PORTFOLIO_IMAGE_NORMAL							:String =  "portfolio image normal";
		public static var PORTFOLIO_IMAGE_SWAP 								:String =  "portfolio image swap";
		
		public static var PORTFOLIO_THUMB_HIDE 								:String =  "portfolio thumb hide";
		public static var PORTFOLIO_THUMB_FADEIN 							:String =  "portfolio thumb fadein";
		public static var PORTFOLIO_THUMB_FADEOUT 							:String =  "portfolio thumb fadeout";
		
		public static var CONTACT_THANKS_MESSAGE							:String =  "Thanks you for contacting us. Someone from our team is going to contact you shortly.";
		public static var CONTACT_ERROR_MESSAGE								:String =  "Oh no! Something is wrong! Please try again later...";
		public static var CONTACT_SENDING_MESSAGE							:String =  "Please wait while the message is being send...";
		public static var CONTACT_TITLE										:String =  "GET IN TOUCH WITH US!";
		public static var CONTACT_TITLE_SENDING								:String =  "SENDING MESSAGE...";
		public static var CONTACT_TITLE_THANKS								:String =  "THANK YOU!";
		
		public static var MENU_HOME_BUTTON 									:String =  "HOME";
		public static var MENU_GAMES_BUTTON 								:String =  "GAMES";
		public static var MENU_WEBSITES_BUTTON 								:String =  "WEBSITES";
		public static var MENU_FLASH_BUTTON 								:String =  "FLASH";
		public static var MENU_HTML_BUTTON 									:String =  "HTML";
		public static var MENU_MOBILE_BUTTON 								:String =  "MOBILE";
		
		public static var FOOTER_CLIENTS_BUTTON 							:String =  "CLIENTS";
		public static var FOOTER_SOLUTIONS_BUTTON 							:String =  "SOLUTIONS";
		public static var FOOTER_CONTACT_BUTTON 							:String =  "CONTACT";
	}
}
