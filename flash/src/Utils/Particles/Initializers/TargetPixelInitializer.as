﻿package Utils.Particles.Initializers {
	import Utils.Particles.ParticleConstants;
	import Utils.Particles.Zones.UniqueBitmapDataZone;

	import org.flintparticles.common.emitters.*;
	import org.flintparticles.common.initializers.*;
	import org.flintparticles.common.particles.*;
	import org.flintparticles.twoD.particles.*;

    public class TargetPixelInitializer extends InitializerBase {
        private var _zone:UniqueBitmapDataZone;

        public function TargetPixelInitializer(param1:UniqueBitmapDataZone) {
            _zone = param1;
            return;
        }
        override public function initialize(param1:Emitter, param2:Particle) : void {
            super.initialize(param1, param2);
            var _loc_3:* = param2 as Particle2D;
            var _loc_4:* = _zone.getLocation();
            _loc_3.dictionary[ParticleConstants.TARGET_POSITION] = _loc_4;
            _loc_3.color = _zone.getColorForLocation(_loc_4);
            return;
        }
    }
}
