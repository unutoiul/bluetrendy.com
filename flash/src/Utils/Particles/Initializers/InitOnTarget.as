﻿package Utils.Particles.Initializers {
	import Utils.Particles.ParticleConstants;

	import org.flintparticles.common.emitters.*;
	import org.flintparticles.common.initializers.*;
	import org.flintparticles.common.particles.*;
	import org.flintparticles.twoD.particles.*;

    public class InitOnTarget extends InitializerBase {

        public function InitOnTarget() {
            return;
        }
        override public function initialize(param1:Emitter, param2:Particle) : void {
            var _loc_3:* = param2 as Particle2D;
            var _loc_4:* = _loc_3.dictionary[ParticleConstants.TARGET_POSITION];
            _loc_3.x = _loc_4.x;
            _loc_3.y = _loc_4.y;
        }
    }
}
