﻿package Utils.Particles.Actions {
	import Utils.Particles.ParticleConstants;

	import org.flintparticles.common.actions.*;
	import org.flintparticles.common.emitters.*;
	import org.flintparticles.common.events.*;
	import org.flintparticles.common.particles.*;
	import org.flintparticles.twoD.particles.*;

	import flash.events.*;
	import flash.geom.*;

    public class TargetPositionGravity extends ActionBase implements IEventDispatcher {
        private var _power:Number;
        private var _dispatcher:EventDispatcher;
        private var _pos:Point;
        private var _gravityConst:Number = 10000;
        private var _epsilonSq:Number;
        private var _hasDispatched:Boolean = true;
        private var _doneCount:int;
        private var _emitter:Emitter;
		public static const REACHED_TARGET : String = "REACHED_TARGET";
		private var _nParticles : uint;

        public function TargetPositionGravity(param1:Number, param2 :Number, param3:Number = 100) {
            _dispatcher = new EventDispatcher(this);
            _power = param1 * _gravityConst;
            _epsilonSq = param3 * param3;
			_nParticles = param2;
			trace("_nParticles "+ _nParticles);
            _pos = new Point();
            return;
        }
        public function dispatchEvent(event:Event) : Boolean {
            return _dispatcher.dispatchEvent(event);
        }
		
        public function get power() : Number {
            return _power / _gravityConst;
        }
		
        override public function update(param1:Emitter, param2:Particle, param3:Number) : void {
           var _loc_4 :Particle2D = Particle2D(param2);
//		   _nPaticles = param1.particles.length;
            if (Particle2D(param2).dictionary[ParticleConstants.TARGET_POSITION] == null){
                return;
            }
            var _loc_5:* = _loc_4.dictionary[ParticleConstants.TARGET_POSITION];
            var _loc_6:* = _loc_4.dictionary[ParticleConstants.TARGET_POSITION].x - _loc_4.x;
            var _loc_7:* = _loc_5.y - _loc_4.y;
            var _loc_8:* = _loc_6 * _loc_6 + _loc_7 * _loc_7;
            if (_loc_6 * _loc_6 + _loc_7 * _loc_7 < 0.5){
                _loc_4.velX = 0;
                _loc_4.velY = 0;
                _loc_4.x = _loc_5.x;
                _loc_4.y = _loc_5.y;
//				//Lucas
          	 	var _loc_11 :TargetPositionGravity = this;
                var _loc_12:int = _doneCount + 1;
                _loc_11._doneCount = _loc_12;
                if (_doneCount == _nParticles && !_hasDispatched){
//                if (_doneCount == param1.particles.length && !_hasDispatched){
                    dispatchEvent(new Event(REACHED_TARGET));
                    _hasDispatched = true;
                }
				return;
            }
            _hasDispatched = false;
            var _loc_9:Number = Math.sqrt(_loc_8);
            if (_loc_8 < _epsilonSq){
                _loc_8 = _epsilonSq;
            }
            var _loc_10:Number = _power * param3 / (_loc_8 * _loc_9);
            _loc_4.velX = _loc_4.velX + _loc_6 * _loc_10;
            _loc_4.velY = _loc_4.velY + _loc_7 * _loc_10;
//            return;
        }
        public function set power(param1:Number) : void {
            _power = param1 * _gravityConst;
            return;
        }
        public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void {
            _dispatcher.removeEventListener(param1, param2, param3);
            return;
        }
        override public function addedToEmitter(param1:Emitter) : void {
            param1.addEventListener(EmitterEvent.EMITTER_UPDATED, onEmitterUpdate, false, 0, true);
            super.addedToEmitter(param1);
            return;
        }
        public function set epsilon(param1:Number) : void {
            _epsilonSq = param1 * param1;
            return;
        }
        public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
            _dispatcher.addEventListener(param1, param2, param3, param4, param5);
            return;
        }
        public function get epsilon() : Number {
            return Math.sqrt(_epsilonSq);
        }
        public function hasEventListener(param1:String) : Boolean {
            return _dispatcher.hasEventListener(param1);
        }
        private function onEmitterUpdate(event:EmitterEvent) : void {
            _doneCount = 0;
			
//            return;
        }
        public function willTrigger(param1:String) : Boolean {
			return _dispatcher.willTrigger(param1);
		}
    }
}
