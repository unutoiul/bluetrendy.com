﻿package  Utils.Particles.Actions{
    import flash.display.*;
    import flash.events.*;
    import org.flintparticles.twoD.actions.*;

    public class MouseDisturbAction extends CombinedActionBase implements IEventDispatcher {
        private var _dispatcher:EventDispatcher;

        public function MouseDisturbAction(param1:DisplayObject, arg_n:Number) {
            _dispatcher = new EventDispatcher(this);
            var _loc_2:TargetPositionGravity = new TargetPositionGravity(600, arg_n);
            _loc_2.addEventListener(TargetPositionGravity.REACHED_TARGET, onComplete);
//			_loc_2.nParticles = arg_n;
            addAction(_loc_2);
            addAction(new MouseAntiGravity(5, param1, 5));
            addAction(new Friction(300));
        }
		
        public function dispatchEvent(event:Event) : Boolean {
            return _dispatcher.dispatchEvent(event);
        }
		
        public function removeEventListener(param1:String, param2:Function, param3:Boolean = false) : void {
            _dispatcher.removeEventListener(param1, param2, param3);
          
        }
        public function hasEventListener(param1:String) : Boolean {
            return _dispatcher.hasEventListener(param1);
        }
        public function willTrigger(param1:String) : Boolean {
            return _dispatcher.willTrigger(param1);
        }
        private function onComplete(event:Event) : void {
            dispatchEvent(new Event(Event.COMPLETE));
        }
        public function addEventListener(param1:String, param2:Function, param3:Boolean = false, param4:int = 0, param5:Boolean = false) : void {
            _dispatcher.addEventListener(param1, param2, param3, param4, param5);
          
        }
    }
}
