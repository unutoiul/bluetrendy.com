﻿package Utils.Particles.Actions{
    import org.flintparticles.common.actions.*;
    import org.flintparticles.common.emitters.*;
    import org.flintparticles.common.particles.*;

    public class CombinedActionBase extends ActionBase {
        private var _actions:Array;

        public function CombinedActionBase() {
           _actions = new Array();
//			trace("CombinedActionBase");
        }
        override public function addedToEmitter(param1:Emitter) : void {
//			trace("CombinedActionBase addedToEmitter");
            var _loc_2:Action = null;
            for each (_loc_2 in _actions){
                _loc_2.addedToEmitter(param1);
            }
			
//			priority = 0;
        }
        protected function addAction(param1:Action) : void {
           _actions.push(param1);
        }
        override public function update(param1:Emitter, param2:Particle, param3:Number) : void {
//			trace("update" + param3);
			var _loc_4:Action = null;
            for each (_loc_4 in _actions){
                _loc_4.update(param1, param2, param3);
            }
        }
    }
}
