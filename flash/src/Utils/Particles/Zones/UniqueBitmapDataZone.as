﻿package Utils.Particles.Zones {
	import org.flintparticles.twoD.particles.Particle2D;
	import org.flintparticles.twoD.zones.*;

	import flash.display.*;
	import flash.geom.*;

    public class UniqueBitmapDataZone extends Object implements Zone2D {
        private var _validPoints:Array;
        private var _height:int;
        private var _width:int;
        private var _bottom:Number;
        private var _pos:Point;
        private var _top:Number;
        private var _bitmapData:BitmapData;
        private var _right:Number;
        private var _left:Number;
        private var _area:Number;
        private var _uniqueValidPoints:Array;

        public function UniqueBitmapDataZone(param1:BitmapData, param2:Number = 0, param3:Number = 0, param4:Number = 0, param5:Number = 0) {
            _bitmapData = param1;
            _left = param4;
            _top = param5;
            _pos = new Point(param2, param3);
            invalidate();
            return;
        }
        public function set y(param1:Number) : void {
            _pos.y = param1;
            return;
        }
        public function invalidate() : void {
            var _loc_2:int = 0;
            var _loc_3:uint = 0;
            _width = _bitmapData.width;
            _height = _bitmapData.height;
            _right = _left + _width;
            _bottom = _top + _height;
            _validPoints = new Array();
            _area = 0;
            var _loc_1:int = 0;
            while (_loc_1 < _width){
                
                _loc_2 = 0;
                while (_loc_2 < _height){
                    
                    _loc_3 = _bitmapData.getPixel32(_loc_1, _loc_2);
                    if ((_loc_3 >> 24 & 255) != 0){
                        var _loc_4 :Object = this;
                        var _loc_5 :int = _area + 1;
                        _loc_4._area = _loc_5;
						
                        _validPoints.push(new Point(_loc_1 + _left, _loc_2 + _top));
                    }
                    _loc_2++;
                }
                _loc_1++;
            }
            _uniqueValidPoints = new Array();
            _uniqueValidPoints = _uniqueValidPoints.concat(_validPoints);
            return;
        }
        public function getLocation() : Point {
            if (_uniqueValidPoints.length == 0){
                _uniqueValidPoints = new Array();
                _uniqueValidPoints = _uniqueValidPoints.concat(_validPoints);
            }
            var _loc_1:* = Math.round(Math.random() * (_uniqueValidPoints.length - 1));
            var _loc_2:* = _uniqueValidPoints.splice(_loc_1, 1)[0] as Point;
            return _loc_2.add(_pos);
        }
        public function get yOffset() : Number {
            return _top;
        }
        public function getArea() : Number {
            return _area;
        }
        public function set xOffset(param1:Number) : void {
            _left = param1;
            invalidate();
            return;
        }
        public function set yOffset(param1:Number) : void {
            _top = param1;
            invalidate();
            return;
        }
        public function get xOffset() : Number {
            return _left;
        }
        public function contains(param1:Number, param2:Number) : Boolean {
            var _loc_3:uint = 0;
            if (param1 >= _left && param1 <= _right && param2 >= _top && param2 <= _bottom){
                _loc_3 = _bitmapData.getPixel32(Math.round(param1 - _left), Math.round(param2 - _top));
                return (_loc_3 >> 24 & 255) != 0;
            }
            return false;
        }
        public function getColorForLocation(param1:Point) : uint {
            var _loc_2:* = param1.subtract(_pos);
            return _bitmapData.getPixel32(_loc_2.x, _loc_2.y);
        }
        public function set bitmapData(param1:BitmapData) : void {
            _bitmapData = param1;
            invalidate();
            return;
        }
        public function set x(param1:Number) : void {
            _pos.x = param1;
            return;
        }
        public function get bitmapData() : BitmapData {
            return _bitmapData;
		}

		public function collideParticle(particle : Particle2D, bounce : Number = 1) : Boolean {
			return false;
		}
    }
}