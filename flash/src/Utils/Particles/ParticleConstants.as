﻿package Utils.Particles{

    public class ParticleConstants extends Object {
        public static const IS_GUIDE:String = "IS_GUIDE";
        public static const TARGET_POSITION:uint = 1;
        public static const GUIDE:String = "GUIDE";
        public static const TARGET_BITMAP:String = "TARGET_BITMAP";

        public function ParticleConstants() {
            return;
        }
    }
}
