﻿package Utils.Particles.Emitter {
	import Utils.Particles.Actions.MouseDisturbAction;
	import Utils.Particles.Initializers.InitOnTarget;
	import Utils.Particles.Initializers.TargetPixelInitializer;
	import Utils.Particles.Zones.UniqueBitmapDataZone;

	import org.flintparticles.common.counters.*;
	import org.flintparticles.common.displayObjects.*;
	import org.flintparticles.common.initializers.*;
	import org.flintparticles.twoD.actions.*;
	import org.flintparticles.twoD.emitters.*;

	import flash.display.*;
	import flash.events.*;

    public class MouseDisturb extends Emitter2D {
        public static const COMPLETE:String = "COMPLETE";
		private var _values:Array;
		
        public function MouseDisturb(param1:UniqueBitmapDataZone, param2:DisplayObject) {
            counter = new Blast(param1.getArea());
			trace("area : " + param1.getArea());
            addInitializer(new ImageClass(Dot,null,false, 1));
            addInitializer(new TargetPixelInitializer(param1));
            addInitializer(new InitOnTarget());
            var _loc_3:MouseDisturbAction = new MouseDisturbAction(param2, param1.getArea());
            _loc_3.addEventListener(Event.COMPLETE, handleActionComplete);
			
            addAction(_loc_3);
//			maximumFrameTime = 100;
//			fixedFrameTime = 100;
			trace("MouseDisturb " + _loc_3.priority);
//            addAction(_loc_3, -100);
            addAction(new Move());
        }
        private function handleActionComplete(event:Event) : void {
            dispatchEvent(new Event(COMPLETE));
        }
		
    }
}
