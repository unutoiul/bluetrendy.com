package Utils {
	import Lookups.BlueTrendyStaticList;

	import Views.Base.ParticleButton;

	import flash.display.BitmapData;
	import flash.display.MovieClip;
	import flash.geom.ColorTransform;
	import flash.geom.Rectangle;
	import flash.text.TextField;
	import flash.text.TextFormat;
	/**
	 * @author LucasOK
	 */
	public class Buttons {
		public function Buttons() :void{
			//constructor
		}
		
		public static function fCreateNormalButton(arg_w :Number, arg_h :Number, arg_round :Number, arg_color: uint, arg_txt :String = "", arg_mc :MovieClip = null) : MovieClip {
			var t_tMc :MovieClip = new MovieClip();
			t_tMc.graphics.beginFill(arg_color);
			t_tMc.graphics.drawRoundRect(0, 0, arg_w, arg_h , arg_round, arg_round);
			t_tMc.graphics.endFill();
			
			var t_rMcArea:Rectangle = new Rectangle(0, 0, t_tMc.width, t_tMc.height);
			
			var t_cAdjustAlpha:ColorTransform = new ColorTransform();
			t_cAdjustAlpha.alphaMultiplier = 0;
			
			var t_bBitmap :BitmapData = new BitmapData(t_tMc.width, t_tMc.height, true);
			t_bBitmap.colorTransform(t_rMcArea, t_cAdjustAlpha);
			t_bBitmap.draw(t_tMc);

			var t_fFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, 11, 0xFFFFFF);
			var t_tTextField :TextField = ApplicationFonts.fCreateText(t_fFormat);
			t_tTextField.text = arg_txt;			
			t_tTextField.mouseEnabled = false;
			
			t_tTextField.x = t_tMc.width/2 - t_tTextField.width/2;
			t_tTextField.y = t_tMc.height/2 - t_tTextField.height/2;
			
			var t_mParticleButton :ParticleButton = new ParticleButton(t_bBitmap);
			var t_mButton:MovieClip = new MovieClip();
			
			
			t_mButton.addChild(t_mParticleButton);
			t_mButton.addChild(t_tTextField);

			if(arg_mc){
				arg_mc.x = arg_w/2 - arg_mc.width/2;
				arg_mc.y = arg_h/2 - arg_mc.height/2;
				
				t_mButton.addChild(arg_mc);
			}
			
			return t_mButton;
		}
		
		public static function fCreateMenuButton(arg_title :String, arg_subtitle :String) : MovieClip {
			var t_tMc :MovieClip = new MovieClip();
			
			var t_fTitleFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, 26, 0x000000);
			var t_fSubTitleFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, 13, BlueTrendyStaticList.COLOR_BLACK_LIGHT);
			
			var t_tTitle :TextField = ApplicationFonts.fCreateText(t_fTitleFormat);
			var t_tSubTitle :TextField = ApplicationFonts.fCreateText(t_fSubTitleFormat);
			
			t_tTitle.text = arg_title;
			t_tSubTitle.text = arg_subtitle;
			
			t_tSubTitle.mouseEnabled = false;
			t_tTitle.mouseEnabled = false;
			
			t_tSubTitle.y = t_tTitle.height - 5;
			
			t_tMc.addChild(t_tTitle);
			t_tMc.addChild(t_tSubTitle);
			
			var t_rMcArea:Rectangle = new Rectangle(0, 0, t_tMc.width, t_tMc.height);
			
			var t_cAdjustAlpha:ColorTransform = new ColorTransform();
			t_cAdjustAlpha.alphaMultiplier = 0;
			
			var t_bBitmap :BitmapData = new BitmapData(t_tMc.width, t_tMc.height, true);
			t_bBitmap.colorTransform(t_rMcArea, t_cAdjustAlpha);
			t_bBitmap.draw(t_tMc);

			
			var t_mParticleButton :ParticleButton = new ParticleButton(t_bBitmap, BlueTrendyStaticList.COLOR_YELLOW);
			var t_mButton:MovieClip = new MovieClip();
			t_mButton.addChild(t_mParticleButton);
			
			t_mButton.id = arg_title;

			return t_mButton;
		}
		
		public static function fCreateIconButton(arg_mc :MovieClip, arg_name :String) : MovieClip {
			var t_tMc :MovieClip = new MovieClip();
			
			t_tMc.addChild(arg_mc);
			
			var t_rMcArea:Rectangle = new Rectangle(0, 0, t_tMc.width, t_tMc.height);
			var t_cAdjustAlpha:ColorTransform = new ColorTransform();

			t_cAdjustAlpha.alphaMultiplier = 0;
			
			var t_bBitmap :BitmapData = new BitmapData(t_tMc.width, t_tMc.height, true);
			t_bBitmap.colorTransform(t_rMcArea, t_cAdjustAlpha);
			t_bBitmap.draw(t_tMc);
			
			var t_mParticleButton :ParticleButton = new ParticleButton(t_bBitmap, BlueTrendyStaticList.COLOR_YELLOW);
			var t_mButton:MovieClip = new MovieClip();
			t_mButton.addChild(t_mParticleButton);
			
			t_mButton.id = arg_name;
			
			return t_mButton;
		}
		
		public static function fCreateArrowButton(arg_title :String, arg_b :Boolean) : MovieClip {
			var t_tMc :MovieClip = new MovieClip();
			var t_fTitleFormat :TextFormat = ApplicationFonts.getTextFormat(ApplicationFonts.fFuturaC, BlueTrendyStaticList.PAGE_BUTTON_FONT_SIZE, 0x000000);
			var t_tTitle :TextField = ApplicationFonts.fCreateText(t_fTitleFormat);
			var t_mArrow :arrowSymbol = new arrowSymbol();
			
			t_tTitle.text = arg_title;
			t_tTitle.mouseEnabled = false;
			
			t_mArrow.rotation = 0;
			
			t_tTitle.x = t_mArrow.width+BlueTrendyStaticList.PORTFOLIO_TEXT_MARGIN;
			
			t_mArrow.x = t_mArrow.width/2;
			t_mArrow.y = t_tTitle.height/2;
			
			t_tMc.addChild(t_tTitle);
			t_tMc.addChild(t_mArrow);
			
			var t_rMcArea:Rectangle = new Rectangle(0, 0, t_tMc.width, t_tMc.height);
			var t_cAdjustAlpha:ColorTransform = new ColorTransform();

			t_cAdjustAlpha.alphaMultiplier = 0;
			
			var t_bBitmap :BitmapData = new BitmapData(t_tMc.width, t_tMc.height, true);
			t_bBitmap.colorTransform(t_rMcArea, t_cAdjustAlpha);
			t_bBitmap.draw(t_tMc);
			
			var t_mParticleButton :ParticleButton = new ParticleButton(t_bBitmap, BlueTrendyStaticList.COLOR_YELLOW);
			var t_mButton:MovieClip = new MovieClip();
			t_mButton.addChild(t_mParticleButton);
			
			t_mButton.id = arg_title;
			
			return t_mButton;
		}
	}
}
